#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <Eigen/Dense>
#include <vector>

#include "io_management/payload.hpp"
#include "pulsar_simulation/antenna.hpp"

namespace py = pybind11;


// Function signature now returns a tuple with three elements
std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, std::map<std::string, int>>
randomize_antenna_output(std::vector<double> freqs, Eigen::MatrixXd dataframe, std::vector<double> rot_phases,
                         double sensitivity = 0.1, double prob_nbrfi = 1., double prob_bbrfi = 1.) {

    // Update the state of the vectors of the pulsar
    payload cargo(freqs, dataframe, rot_phases);
    antenna antenna_obj(cargo, sensitivity, prob_nbrfi, prob_bbrfi);
    
    // Get the two MatrixXd objects
    std::pair<Eigen::MatrixXd, Eigen::MatrixXd> total_noise_total_flux = antenna_obj.randomize_antenna_output();
    
    // Create the map (Python-style dictionary equivalent in C++)
    std::map<std::string, int> description = {
        {"Pulsar", antenna_obj.pulsar_count},
        {"NBRFI", antenna_obj.nbrfi_count},
        {"BBRFI", antenna_obj.bbrfi_count}
    };

    // Return a tuple containing the two matrices and the description map
    return std::make_tuple(total_noise_total_flux.first, total_noise_total_flux.second, description);
}

PYBIND11_MODULE(_m_signal_detection_module, m) {
    // Binding for process_vectors
    m.def("randomize_antenna_output", &randomize_antenna_output, "returns detected flux in voltage along with added noise",
        py::arg("freqs"), py::arg("dataframe"), py::arg("rot_phases"), py::arg("sensitivity"), py::arg("prob_nbrfi"), py::arg("prob_bbrfi"));

}