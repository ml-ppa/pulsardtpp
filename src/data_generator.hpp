//
// Created by Marcel Trattner on 29.01.24.
//

#ifndef PULSARDTPP_DATA_GENERATOR_HPP
#define PULSARDTPP_DATA_GENERATOR_HPP

#include <vector>
#include <queue>
#include <random>
#include <numbers>
#include <map>
#include <utility>
#include <thread>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <omp.h>

#include "pulsar_simulation/interstellar_medium.hpp"
#include "pulsar_simulation/antenna.hpp"
#include "pulsar_simulation/spark_pattern_generator.hpp"
#include "pulsar_simulation/pulsar_animator.hpp"
#include "common/structures.hpp"
#include "common/calculation.hpp"
#include "io_management/file_writer.hpp"

namespace calc = calculation;

class data_generator {

public:
    pulsar_animator animator;
    std::vector<double> freq_channels;


    data_generator(pulsar_animator anim = pulsar_animator());

    std::pair<Eigen::MatrixXd, Eigen::MatrixXd>
    generate_data_spark_pattern(const std::vector<double>& rot_phases, std::vector<double> freq_channel, std::string tag = "test",
                                double signal_time_bin = 0.09166, const std::string& param_folder="./params/runtime/", const std::string& payload_folder="./params/payloads/",
                                double antenna_sensitivity=0.5, double prob_nbrfi=0.5, double prob_bbrfi=0.5, double scale_direction_randomness=0.5,
                                bool remove_drift_effect=true);

    std::vector<shared_ptr<spark_pattern_generator::spark>>
    generate_random_spark_pattern(
            aggregated_payload& aggregate, int num_cones = 2, double avg_spark_dimension = 7.0, float avg_spark_per_cone_length = .05,
            double avg_spark_pattern_center_tilt = 45.0, double avg_drift_vel = 1.);

    payload generate_payload_at_acquisition_frequency(const payload& payload_raw) const;

    interstellar_medium generate_random_ism_obj(aggregated_payload& aggregate);
};


#endif //PULSARDTPP_DATA_GENERATOR_HPP
