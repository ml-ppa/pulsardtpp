//
// Created by Marcel Trattner on 01.02.24.
//

#ifndef PULSARDTPP_QUATERNION_HPP
#define PULSARDTPP_QUATERNION_HPP

#include <vector>
#include <algorithm>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include "calculation.hpp"


class quaternion {
private:
    Eigen::Quaterniond q;

public:
    quaternion(std::vector<double> axis, double angle) {
        Eigen::Vector3d z_axis_rotFrame  = {axis[0], axis[1], axis[2]};
        Eigen::AngleAxis aa(angle, z_axis_rotFrame);

        q = Eigen::Quaterniond(aa);
    }

    quaternion() {

    }

    std::vector<double> rotate(std::vector<double> vector) {
        Eigen::Vector3d v(vector[0], vector[1], vector[2]);

        Eigen::Quaterniond p;
        p.w() = 0;
        p.vec() = v;

        Eigen::Quaterniond rotatedP = q * p * q.inverse();
        Eigen::Vector3d rotatedV = rotatedP.vec();

        return {&rotatedV[0], rotatedV.data()+rotatedV.cols()*rotatedV.rows()};
    }

    Eigen::Vector3d get_vector() {
        return q.vec();
    }

    Eigen::Quaterniond get_q() {
        return q;
    }
};


#endif //PULSARDTPP_QUATERNION_HPP
