//
// Created by Marcel Trattner on 30.01.24.
//

#ifndef PULSARDTPP_REFERENCE_FRAME_OPERATION_HPP
#define PULSARDTPP_REFERENCE_FRAME_OPERATION_HPP

#include <vector>
#include <iostream>
#include <numbers>
#include "calculation.hpp"
#include "quaternion.hpp"

namespace reference_frame_operation
{
    // Defined structs for return values
    struct rot_frame_basis_vectors {
        std::vector<std::vector<double>> new_rot_frame_basis = {{}};
        std::vector<std::vector<double>> rot_frame_basis = {{}};
        quaternion rot_frame_quaternion;
    };

    struct mag_pole_rot_frame {
        std::vector<double> mag_pol_position_rot_frame_cartesian;
        std::vector<double> mag_pol_position_celestial_frame_cartesian;
        std::vector<double> mag_pol_position_rot_frame_spherical;
        std::vector<double> mag_pol_position_celestial_frame_spherical;
        std::vector<std::vector<double>> mag_frame_basis_vectors;
    };

    struct earth_pos_in_pulsar_frame {
        std::vector<double> earth_rot_frame;
        std::vector<double> vector_to_earth_dir;
        std::vector<double> earth_rot_frame_spherical;
        std::vector<double> vector_to_earth_dir_spherical;
    };

    // Helper functions
    inline std::vector<double> transform2spherical(const std::vector<double>& cartesian_coor) {
        double radius = pow(pow(cartesian_coor[0], 2) + pow(cartesian_coor[1], 2) + pow(cartesian_coor[2], 2), .5);
        double declination = acos(cartesian_coor[2] / radius) * 180 / (22.0/7.0);
        double azimuth = 0;
        if (cartesian_coor[0] != 0 && cartesian_coor[1] != 0)
            azimuth = copysign(1, cartesian_coor[1]) *
                    acos(cartesian_coor[0] / sqrt(pow( cartesian_coor[0], 2) + pow( cartesian_coor[1], 2))) * 180.0/(22.0/7.0);
        return {radius, declination, azimuth};
    }

    inline std::vector<double> transform2cartesian(const std::vector<double>& spherical_coor) {
        double radius = spherical_coor[0];
        double declination = spherical_coor[1] * 22.0/7.0/180.0;
        double azimuth = spherical_coor[2] * 22.0/7.0/180.0;

        return {
            radius * sin(declination) * cos(azimuth),
            radius * sin(declination) * sin(azimuth),
            radius * cos(declination)
        };
    }

    inline Eigen::Vector3d polar2cart(double r, double theta, double phi) {
        return {
                r * sin(theta) * cos(phi),
                r * sin(theta) * sin(phi),
                r * cos(theta)
        };
    }

    // Function to rotate a reference frame using a quaternion
    inline std::vector<Eigen::Vector3d> rotate_reference_frame(
            const std::vector<Eigen::Vector3d>& frame_0_basis,
            const Eigen::Vector3d& axis_of_rotation,
            double rotation_angle)
    {
        // Create the quaternion representing the rotation
        Eigen::AngleAxisd rotation_vector(rotation_angle, axis_of_rotation.normalized());
        Eigen::Quaterniond quaternion_rotator(rotation_vector);

        // Rotate each vector in the frame_0_basis using the quaternion
        std::vector<Eigen::Vector3d> frame_1_basis;
        frame_1_basis.reserve(frame_0_basis.size());
        for (const auto& vec : frame_0_basis) {
            frame_1_basis.push_back(quaternion_rotator * vec);
        }

        return frame_1_basis;
    }

    inline std::vector<double> multiply_components_with_basis_vectors(const std::vector<double>& components, const std::vector<std::vector<double>>& basis_vectors) {
        const double& x_component = components[0];
        const double& y_component = components[1];
        const double& z_component = components[2];

        const std::vector<double>& x_basis = basis_vectors[0];
        const std::vector<double>& y_basis = basis_vectors[1];
        const std::vector<double>& z_basis = basis_vectors[2];

        std::vector<double> part_x(3,0);
        std::vector<double> part_y(3,0);
        std::vector<double> part_z(3,0);

        for (int i = 0; i < 3; ++i) {
            part_x[i] = x_component * x_basis[i];
            part_y[i] = y_component * y_basis[i];
            part_z[i] = z_component * z_basis[i];
        }

        std::vector<double> total_vector(3,0);
        for (int i = 0; i < 3; ++i) {
            total_vector[i] = part_x[i] + part_y[i] + part_z[i];
        }
        return total_vector;
    }

    // Reference frame operations
    inline rot_frame_basis_vectors calculate_pulsar_rot_frame_basis_vectors(const std::vector<double>& pulsar_rotation_axis, double& rot_phase) {
        if(pulsar_rotation_axis.size() < 3)
            std::cout << "ERROR: pulsar_rotation_axis vector needs to have three dimensions.\n";
        double pulsar_rotation_axis_mag = calculation::vector_magnitude(pulsar_rotation_axis);

        std::vector<double> z_axis_rotFrame;
        z_axis_rotFrame.reserve(pulsar_rotation_axis.size());
        for (double dim : pulsar_rotation_axis) {
            z_axis_rotFrame.push_back(dim / pulsar_rotation_axis_mag);
        }

        std::vector<double> x_axis_rotFrame;
        if( !(z_axis_rotFrame == std::vector<double>{0., 0., 1.})) {
            x_axis_rotFrame = calculation::cross_product(z_axis_rotFrame, {0., 0., 1.});
            double magnitude = calculation::vector_magnitude(x_axis_rotFrame);
            for (double dim : x_axis_rotFrame) {
                dim = dim / magnitude;
            }
        } else {
            x_axis_rotFrame = {1., 0., 0.};
        }

        std::vector<double> y_axis_rotFrame = calculation::cross_product(z_axis_rotFrame, x_axis_rotFrame);
        double magnitude = calculation::vector_magnitude(y_axis_rotFrame);
        for (double dim : y_axis_rotFrame) {
            dim = dim / magnitude;
        }
        std::vector<std::vector<double>> rot_frame_basis = {x_axis_rotFrame, y_axis_rotFrame, z_axis_rotFrame};
        quaternion rot_frame_quaternion = quaternion(z_axis_rotFrame, 2 * std::numbers::pi * rot_phase);

        std::vector<std::vector<double>> new_rot_frame_basis;
        new_rot_frame_basis.reserve(rot_frame_basis.size());
        for (const std::vector<double>& frame : rot_frame_basis) {
            new_rot_frame_basis.emplace_back(rot_frame_quaternion.rotate(frame));
        }

        return {new_rot_frame_basis, rot_frame_basis, rot_frame_quaternion};
    }

    inline mag_pole_rot_frame calculate_mag_pole_in_rot_frame(const std::vector<std::vector<double>> &rot_frame_basis_vectors, float mag_axis_tilt) {
        quaternion to_tilt_mag_axis_quaternion = quaternion(rot_frame_basis_vectors[0], mag_axis_tilt/180.0 * 22.0/7.0);
        std::vector<double> mag_pol_in_celestial_frame = to_tilt_mag_axis_quaternion.rotate(rot_frame_basis_vectors[2]);

        const std::vector<double>& z_axis_mag_frame = mag_pol_in_celestial_frame;
        const std::vector<double>& x_axis_mag_frame = rot_frame_basis_vectors[0];
        const std::vector<double> y_axis_mag_frame = to_tilt_mag_axis_quaternion.rotate(rot_frame_basis_vectors[1]);

        const std::vector<double>& x_axis_rot_frame = rot_frame_basis_vectors[0];
        const std::vector<double>& y_axis_rot_frame = rot_frame_basis_vectors[1];
        const std::vector<double>& z_axis_rot_frame = rot_frame_basis_vectors[2];

        std::vector<double> mag_pol_rot_frame = {
                (
                        x_axis_rot_frame[0] * mag_pol_in_celestial_frame[0] +
                        x_axis_rot_frame[1] * mag_pol_in_celestial_frame[1] +
                        x_axis_rot_frame[2] * mag_pol_in_celestial_frame[2]),
                (
                        y_axis_rot_frame[0] * mag_pol_in_celestial_frame[0] +
                        y_axis_rot_frame[1] * mag_pol_in_celestial_frame[1] +
                        y_axis_rot_frame[2] * mag_pol_in_celestial_frame[2]),
                (
                        z_axis_rot_frame[0] * mag_pol_in_celestial_frame[0] +
                        z_axis_rot_frame[1] * mag_pol_in_celestial_frame[1] +
                        z_axis_rot_frame[2] * mag_pol_in_celestial_frame[2]
                )
        };
        return {mag_pol_rot_frame, mag_pol_in_celestial_frame,
                transform2spherical(mag_pol_rot_frame), transform2spherical(mag_pol_in_celestial_frame),
                {x_axis_mag_frame, y_axis_mag_frame, z_axis_mag_frame}};
    }

    inline earth_pos_in_pulsar_frame calculate_earth_pos_in_pulsar_frame(const std::vector<double>& pulsar_celestial_coor, const std::vector<std::vector<double>>& rot_frame_basis_vectors) {
        std::vector<double> vector_to_pulsar_from_earth = transform2cartesian(pulsar_celestial_coor);
        std::vector<double> vector_to_earth_from_pulsar = {-vector_to_pulsar_from_earth[0], -vector_to_pulsar_from_earth[1], -vector_to_pulsar_from_earth[2]};
        double vector_to_earth_from_pulsar_mag = pow( pow( vector_to_earth_from_pulsar[0], 2) +
                                                              pow( vector_to_earth_from_pulsar[1], 2) +
                                                              pow( vector_to_earth_from_pulsar[2], 2), .5);
        std::vector<double> vector_to_earth_dir  = {
                vector_to_earth_from_pulsar[0] / vector_to_earth_from_pulsar_mag,
                vector_to_earth_from_pulsar[1] / vector_to_earth_from_pulsar_mag,
                vector_to_earth_from_pulsar[2] / vector_to_earth_from_pulsar_mag
        };

        const std::vector<double>& x_axis_rot_frame = rot_frame_basis_vectors[0];
        const std::vector<double>& y_axis_rot_frame = rot_frame_basis_vectors[1];
        const std::vector<double>& z_axis_rot_frame = rot_frame_basis_vectors[2];

        std::vector<double> earth_rot_frame = {
                x_axis_rot_frame[0]*vector_to_earth_dir[0] + x_axis_rot_frame[1]*vector_to_earth_dir[1] + x_axis_rot_frame[2]*vector_to_earth_dir[2],
                y_axis_rot_frame[0]*vector_to_earth_dir[0] + y_axis_rot_frame[1]*vector_to_earth_dir[1] + y_axis_rot_frame[2]*vector_to_earth_dir[2],
                z_axis_rot_frame[0]*vector_to_earth_dir[0] + z_axis_rot_frame[1]*vector_to_earth_dir[1] + z_axis_rot_frame[2]*vector_to_earth_dir[2]
        };

        return {
                earth_rot_frame,
                vector_to_earth_dir,
                transform2spherical(earth_rot_frame),
                transform2spherical(vector_to_earth_dir)
        };
    }
}

#endif //PULSARDTPP_REFERENCE_FRAME_OPERATION_HPP
