//
// Created by Marcel Trattner on 08.08.23.
//

#ifndef PULSARDTPP_STRUCTURES_HPP
#define PULSARDTPP_STRUCTURES_HPP

#include <utility>
#include <vector>

struct tuple_vec{
    std::vector<double> a;
    std::vector<double> b;
    tuple_vec(std::vector<double> v1, std::vector<double> v2) : a(std::move(v1)), b(std::move(v2)) {}
};

struct params {
    std::vector<double> rot_phases;
    std::vector<double> pulsar_celestial_coors = {1,45,0};
    std::vector<double> pulsar_rotation_axis = {0,0,1};
    double mag_axis_tilt = 45.0;
    std::vector<double> conal_azimuths = {0,90,180,270};
    std::vector<double> conal_declinations = {25,25,25,25};
    std::vector<double> rad_conal = {5,5,5,5};
    double rad_core = 5.0;
    float scintillation_index_homo = .5;
    std::vector<double> freq_list = {1};
    double dm_homogeneous = 1.0;
    float pulsar_period = 1.0;
    double T_S = 1.0;
    float bandwidth = 1.0;
    float acquisition_time = 1.0;
    double time_of_occurrence_BBRFI = .5;
    std::vector<int> channels_effected_rangeBBRFI = {1,100};
    float spread_in_phaseBBRFI = .01;
    double amplitudeBBRFI = 1.0;
    float probability_of_occurrence_BBRFI = 0.8;
    double center_channel_indexNBRFI = 100.0;
    float spread_in_channelsNBRFI = 10.0;
    double amplitudeNBRFI = 1.0;
    float probability_of_occurrence_NBRFI = 0.8;
};

#endif //PULSARDTPP_STRUCTURES_HPP
