#ifndef HEADER_CALCULATION
#define HEADER_CALCULATION

#include <vector>
#include <cmath>
#include <numeric>
#include <random>
#include <iostream>
#include <Eigen/Dense>
#include <chrono>

using namespace std;

namespace calculation
{
    namespace // Anonymous namespace to cluster internal helper functions
    {
        vector<double> convolution_algorithm(const vector<double> &a, const vector<double> &v)
        {
            // Save length of the two arrays
            int N = a.size();
            int M = v.size();
            // Account for even or odd length of mask
            short int even_odd;
            M % 2 == 0 ? even_odd = 1 : even_odd = 0;

            int m;
            double sum;
            vector<double> result;
            for (int i = 0; i < N; i++)
            {
                sum = 0;
                m = (int) M-1;
                for (int j = (int)(i - (M / 2)); j <= (int)(i + (M / 2) - even_odd); j++)
                {
                    if (j < 0 || j >= N)
                        m--;
                    else
                        sum += a.at(j) * v.at(m--);
                }

                result.push_back(sum);
            }
            return result;
        }
    }
    /*
        Multiplies two matrices or matrix and vector of arbitrary size.
    */
    inline void matrix_mult(double* mat_a, double* mat_b, int rows_a, int cols_a, int rows_b, int cols_b, double* rslt_mat)
    {
        if(cols_a != rows_b) 
        {
            std::cout << "ERROR: dimensions of matrices do not match. Matrix has " + std::to_string(cols_a) + 
            " columns and Matrix B has " + std::to_string(rows_b) + " rows" << std::endl;
            return;
        }

        for (int i = 0; i < rows_a; i++) {
            for (int j = 0; j < cols_b; j++) {
                for (int k = 0; k < rows_b; k++) {
                    *(rslt_mat + i * cols_b + j) += *(mat_a + i * cols_a + k) * *(mat_b + k * cols_b + j);
                }
            }
        }
    }

    /*
        Multiplies a vector with a scalar value.
    */
    inline void scalar_vector_mult( double* vector, double scalar, int size, double* rslt_vec )
    {
        for (int i = 0; i < size; i++)
        {
            *(rslt_vec + i) = *(vector + i) * scalar;
        }
        
    }

    /*
     * Calculates the dot product of two vectors. Size has to be provided to make this usable for all ranges.
     */
    inline double dot_product_parray(const double* a, const double* b, int size)
    {
        double product = 0;
        for (int i = 0; i < size; ++i) {
            product += a[i] * b[i];
        }

        return product;
    }

    /*
     * Calculates the cross product of two vectors in three-dimensional space
     */
    inline vector<double> cross_product(const vector<double> &a, const vector<double> &b)
    {
        if(a.size() != 3 || b.size() != 3)
            throw std::out_of_range ("ERROR: Dimension of both vectors to calculate cross product has to be 3.\n");

        return vector<double> {
                a[1]*b[2] - a[2]*b[1],
                a[2]*b[0] - a[0]*b[2],
                a[0]*b[1] - a[1]*b[0]
        };
    }

    /*
     * Calculates the magnitude of a three-dimensional vector
     */
    inline double vector_magnitude(const vector<double> &vec)
    {
        if(vec.size() != 3)
            throw std::out_of_range ("ERROR: Dimension of the input vector to calculate its magnitude has to be 3.\n");

        return sqrt(pow(vec[0], 2) + pow(vec[1], 2) + pow(vec[2], 2));
    }

    /*
        Discrete convolution of two vectors
    */
    inline vector<double> convolve(vector<double> a, vector<double> v)
    {
        if(a.empty() || v.empty())
            throw std::out_of_range ("ERROR: Vectors cannot be empty.\n");

        if (a.size() >= v.size())
        {
            return convolution_algorithm(a, v);
        } else
        {
            return convolution_algorithm(v, a);
        }
    }

    /*
        Creates a vector filled with values between the two specified numbers and step.
    */
    inline vector<double> arange(double start, double stop, double step)
    {
        double length = stop - start;
        int size = int (std::round( length/step ));
        if( size < 0 ) size = size * -1;
        if (size == 0) size = 1;
        // Condition when stopping limit is lower than the start and the step is positive, modeled after numpy.arange
        if(stop < start && step > 0) return vector<double>{};

        vector<double> vec(size, start);

        double tmp = start;
        for (int i = 1; i < size; i++)
        {
            tmp += step;
            vec.at(i) = tmp;
        }

        return vec;
    }

    inline double uniform_real_distribution(double min = 0.0, double max = 1.0) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(min, max);
        return dis(gen);
    }

    /*
        Return a random value using Gaussian normal distribution
     */
    inline double normal_distribution(double mean, double stddev)
    {
        static std::default_random_engine generator(
                std::chrono::system_clock::now().time_since_epoch().count()
                );
        std::normal_distribution<double> distribution( mean, stddev );

        return distribution(generator);
    }

    /*
        Return a vector with a gaussian distribution
    */
    inline vector<double> normal_distribution(double mean, double stddev, int size)
    {
        vector<double> vec(size, 0);
        for (int i=0; i<size; ++i) {
            vec.at(i) = normal_distribution(mean, stddev);
        }

        return vec;
    }

    inline vector<double> random_vector(int size, double factor)
    {
        vector<double> vec(size, 0);
        for (int i = 0; i < size; i++)
        {
            vec.at(i) = ((float) rand()/RAND_MAX) * factor;
        }
        return vec;
    }

    inline void normalize_vector(double* vector)
    {
        double denominator = pow((
                         pow(vector[0], 2) +
                         pow(vector[1], 2) +
                         pow(vector[2], 2)
                 ), 0.5);
        vector[0] = vector[0] / denominator;
        vector[1] = vector[1] / denominator;
        vector[2] = vector[2] / denominator;
    }

    using Eigen::VectorXd;
    inline VectorXd interp_func(const VectorXd& x, const VectorXd& xp, const VectorXd& fp,
                                double left = NAN, double right = NAN) {
        int n = xp.size();
        Eigen::VectorXd y(x.size());

        for (int i = 0; i < x.size(); ++i) {
            // Handle cases where x is out of bounds of xp
            if (x[i] <= xp[0]) {
                y[i] = isnan(left) ? fp[0] : left;
            } else if (x[i] >= xp[n - 1]) {
                y[i] = isnan(right) ? fp[n - 1] : right;
            } else {
                // Linear interpolation
                for (int j = 0; j < n - 1; ++j) {
                    if (x[i] >= xp[j] && x[i] <= xp[j + 1]) {
                        double t = (x[i] - xp[j]) / (xp[j + 1] - xp[j]);
                        y[i] = fp[j] + t * (fp[j + 1] - fp[j]);
                        break;
                    }
                }
            }
        }
        return y;
    }

    using Eigen::VectorXd;
    inline VectorXd interp(const VectorXd& x, const VectorXd& xp, const VectorXd& fp,
                           double left = NAN, double period = NAN, double right = NAN) {

        // Check for length mismatch
        if (xp.size() != fp.size()) {
            throw std::invalid_argument("xp and fp must be the same length.");
        }

        // Output vectors initialized to input size
        VectorXd x_out = x;
        VectorXd xp_out = xp;
        VectorXd fp_out = fp;

        if (!isnan(period)) {
            // Check if period is zero
            if (period == 0) {
                throw std::invalid_argument("period must be a non-zero value");
            }
            left = NAN;
            right = NAN;

            double abs_period = std::abs(period);

            // Modulo operation for normalization within period
            auto mod_period = [&](double val) {
                double result = std::fmod(val, abs_period);
                return (result < 0) ? result + abs_period : result;
            };

            x_out = x_out.unaryExpr(mod_period);
            xp_out = xp_out.unaryExpr(mod_period);

            // Sorting xp and rearranging fp to match sorted xp indices
            vector<int> sorted_indices(xp_out.size());
            std::iota(sorted_indices.begin(), sorted_indices.end(), 0);

            std::sort(sorted_indices.begin(), sorted_indices.end(),
                      [&](int a, int b) { return xp_out[a] < xp_out[b]; });

            VectorXd xp_sorted(xp_out.size());
            VectorXd fp_sorted(fp_out.size());

            for (size_t i = 0; i < sorted_indices.size(); ++i) {
                xp_sorted[i] = xp_out[sorted_indices[i]];
                fp_sorted[i] = fp_out[sorted_indices[i]];
            }

            xp_out = xp_sorted;
            fp_out = fp_sorted;

            // Extend xp and fp for periodic boundary conditions
            VectorXd xp_extended(xp_out.size() + 2);
            VectorXd fp_extended(fp_out.size() + 2);

            xp_extended << xp_out[xp_out.size() - 1] - abs_period, xp_out, xp_out[0] + abs_period;
            fp_extended << fp_out[fp_out.size() - 1], fp_out, fp_out[0];

            xp_out = xp_extended;
            fp_out = fp_extended;
        }

        return calculation::interp_func(x_out, xp_out, fp_out, left, right);
    }

    inline std::vector<std::vector<double>> transpose(const std::vector<std::vector<double>>& matrix) {
        if (matrix.empty()) return {};

        size_t rows = matrix.size();
        size_t cols = matrix[0].size();
        std::vector<std::vector<double>> transposed(cols, std::vector<double>(rows));

        for (size_t i = 0; i < rows; ++i) {
            for (size_t j = 0; j < cols; ++j) {
                transposed[j][i] = matrix[i][j];
            }
        }

        return transposed;
    }
}

#endif // HEADER_CALCULATION