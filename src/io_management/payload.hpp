//
// Created by Marcel Trattner on 01.08.24.
//

#ifndef PULSARDTPP_PAYLOAD_HPP
#define PULSARDTPP_PAYLOAD_HPP


#include <vector>
#include <iostream>
#include <map>
#include <Eigen/Dense>

class payload {
public:
    struct description {
        int Pulsar = 0;
        int NBRFI =  0;
        int BBRFI = 0;
    };

    std::vector<double> freqs;
    Eigen::MatrixXd dataframe;
    std::vector<double> rot_phases; // might change to int
    description desc = description();

    payload(const std::vector<double>& freqs) : freqs(freqs) {}
    payload(std::vector<double> freqs, Eigen::MatrixXd dataframe, std::vector<double> rot_phases) : freqs(freqs), dataframe(dataframe), rot_phases(rot_phases) {}

    void add_flux(const std::pair<std::vector<double>, std::vector<double>>& radio_packet) {
        std::vector<double> flux_received = radio_packet.first;
        std::vector<double> freqs_received = radio_packet.second;

        // Check if frequency channels match
        double max_diff = 0.0;
        for (size_t i = 0; i < freqs_received.size(); ++i) {
            max_diff = std::max(max_diff, std::abs(freqs_received[i] - freqs[i]));
        }

        if (max_diff == 0.0) {
            // Map flux_received to an Eigen::RowVectorXd
            Eigen::RowVectorXd row = Eigen::Map<Eigen::RowVectorXd>(flux_received.data(), flux_received.size());

            // Resize the matrix to add a new row (make sure dataframe is Eigen::MatrixXd)
            if (dataframe.cols() == 0) {
                dataframe.conservativeResize(dataframe.rows() + 1, flux_received.size());
            } else {
                dataframe.conservativeResize(dataframe.rows() + 1, Eigen::NoChange);
            }

            // Add the new row
            dataframe.row((dataframe.rows() - 1)) = row;
        } else {
            throw std::range_error("Freq channels don't match assigned channels");
        }
    }

    void assign_rot_phases(const std::vector<double>& rot_phases) {
        this->rot_phases = rot_phases;
    }
};

class runtime{
public:
    std::vector<int> num_sparks;
    std::vector<double> conal_latitudes;
    Eigen::Vector2d spark_rotation_axis_polar_att0;
    double drift_vel;
    double spark_dimension;
    std::vector<double> spark_mid_freqs;
    std::vector<double> spark_flux_powers;
    Eigen::Vector3d direction;
    double dm_homogeneous;
    double scintillation_index_homogeneous;
    double std_dm;
    double std_scintillation_index;
};

class aggregated_payload {
public:
    std::string tag = "test";

    enum LOAD_STATE {RAW, TO_BE_PROPAGATED, DETECTED, FLUX};
    std::map<LOAD_STATE, payload> haul;
    runtime runtime_params;

    aggregated_payload(std::string tag) : tag(tag) {}

    void add(LOAD_STATE state, const payload& cargo) {
        std::pair<LOAD_STATE, payload> payload_raw(state, cargo);
        haul.insert(payload_raw);
    };
};

#endif //PULSARDTPP_PAYLOAD_HPP