//
// Created by Marcel Trattner on 11.09.24.
//

#ifndef PULSARDTPP_FILE_WRITER_HPP
#define PULSARDTPP_FILE_WRITER_HPP

#include <iostream>
#include <fstream>
#include <filesystem>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include "payload.hpp"

namespace fs = std::filesystem;

class file_writer {
private:
    // Function to write a 2D std::vector to a JSON array
    template <typename T>
    static rapidjson::Value convert_vector_to_json(const std::vector<T>& vec, rapidjson::Document::AllocatorType& allocator) {
        rapidjson::Value array(rapidjson::kArrayType);  // Initialize array

        for (const auto& element : vec) {
            array.PushBack(element, allocator);  // Add the element
        }

        return array;
    }

    // Function to write an Eigen::MatrixXd to a JSON array
    static rapidjson::Value convert_matrix_to_json(const Eigen::MatrixXd& mat, rapidjson::Document::AllocatorType& allocator) {
        rapidjson::Value array(rapidjson::kArrayType);  // Outer array

        for (int i = 0; i < mat.rows(); ++i) {
            rapidjson::Value rowArray(rapidjson::kArrayType);  // Inner array for each row
            for (int j = 0; j < mat.cols(); ++j) {
                rowArray.PushBack(mat(i, j), allocator);  // Push each matrix element to the inner array
            }
            array.PushBack(rowArray, allocator);  // Add the row to the outer array
        }

        return array;
    }

    static std::string convert_relative_path(const std::string& filepath) {
        fs::path user_path(filepath);
        if (user_path.is_relative()) {
            fs::path absolute_path = fs::absolute(user_path);
            return "Absolute path: " + absolute_path.string();
        } else {
            return "";
        }
    }

public:
    static void write_testrun(const aggregated_payload& aggregate, const std::string& param_folder="./params/runtime/", const std::string& payload_folder="./params/payloads/") {
        file_writer::write_payload_to_file(aggregate.haul.at(
                aggregated_payload::LOAD_STATE::RAW), payload_folder + aggregate.tag + "_payload_raw.json");
        file_writer::write_payload_to_file(aggregate.haul.at(
                aggregated_payload::LOAD_STATE::TO_BE_PROPAGATED), payload_folder + aggregate.tag + "_payload_tobe_propagated.json");
        file_writer::write_payload_to_file(aggregate.haul.at(
                aggregated_payload::LOAD_STATE::DETECTED), payload_folder + aggregate.tag + "_payload_detected.json");
        file_writer::write_payload_to_file(aggregate.haul.at(
                aggregated_payload::LOAD_STATE::FLUX), payload_folder + aggregate.tag + "_payload_flux.json");

        file_writer::write_runtime_to_file(
                aggregate, param_folder + aggregate.tag + ".json");
    }

    static void write_payload_to_file(const payload& payload, const std::string& filePath) {
        // Create a RapidJSON Document object
        rapidjson::Document document;
        document.SetObject();  // Set as an empty JSON object

        // To add data, you need a rapidjson allocator
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        // Add the std::vector as a JSON array
        rapidjson::Value freqs = convert_vector_to_json(payload.freqs, allocator);
        document.AddMember("freqs", freqs, allocator);

        // Add the Eigen::MatrixXd as a JSON array
        rapidjson::Value dataframe = convert_matrix_to_json(payload.dataframe, allocator);
        document.AddMember("dataframe", dataframe, allocator);

        // Bandwidth
        document.AddMember("bandwidths", rapidjson::Value(rapidjson::kNullType), allocator);

        // Description
        rapidjson::Value description(rapidjson::kObjectType);
        description.AddMember("Pulsar", payload.desc.Pulsar, allocator);
        description.AddMember("NBRFI", payload.desc.NBRFI, allocator);
        description.AddMember("BBRFI", payload.desc.BBRFI, allocator);
        document.AddMember("description", description, allocator);

        // Add the std::vector as a JSON array
        rapidjson::Value rot_phases = convert_vector_to_json(payload.rot_phases, allocator);
        document.AddMember("rot_phases", rot_phases, allocator);

        // Convert the JSON document to a string (stringify it)
        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);  // Write the JSON document into the buffer

        // Now write the stringified JSON to a file
        std::ofstream ofs(filePath);
        if (ofs.is_open()) {
            ofs << buffer.GetString();  // Write the JSON string to the file
            ofs.close();
        } else {
            std::cerr << "Failed to open the file: " + filePath + " for writing. "  << convert_relative_path(filePath) << std::endl;
        }
    }

    static void write_runtime_to_file(const aggregated_payload& aggregate, const std::string& filePath) {
        // Create a RapidJSON Document object
        rapidjson::Document document;
        document.SetObject();  // Set as an empty JSON object

        // To add data, you need a rapidjson allocator
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        // Create a JSON array and adding num_sparks to the document
        rapidjson::Value num_sparks = convert_vector_to_json(aggregate.runtime_params.num_sparks, allocator);
        document.AddMember("num_sparks", num_sparks, allocator);

        // Create a JSON array and adding conal_latitudes to the document
        rapidjson::Value conal_latitudes = convert_vector_to_json(aggregate.runtime_params.conal_latitudes, allocator);
        document.AddMember("conal_latitudes", conal_latitudes, allocator);

        // Create a JSON array and adding conal_latitudes to the document
        rapidjson::Value spark_rotation_axis_polar_att0(rapidjson::kArrayType);  // Create an empty array
        for (double element : aggregate.runtime_params.spark_rotation_axis_polar_att0) {
            spark_rotation_axis_polar_att0.PushBack(element, allocator);
        }
        document.AddMember("spark_rotation_axis_polar_att0", spark_rotation_axis_polar_att0, allocator);

        document.AddMember("drift_vel", aggregate.runtime_params.drift_vel, allocator);

        document.AddMember("spark_dimension", aggregate.runtime_params.spark_dimension, allocator);

        // Create a JSON array and adding conal_latitudes to the document
        rapidjson::Value spark_mid_freqs = convert_vector_to_json(aggregate.runtime_params.spark_mid_freqs, allocator);
        document.AddMember("spark_mid_freqs", spark_mid_freqs, allocator);

        // Create a JSON array and adding conal_latitudes to the document
        rapidjson::Value spark_flux_powers = convert_vector_to_json(aggregate.runtime_params.spark_flux_powers, allocator);
        document.AddMember("spark_flux_powers", spark_flux_powers, allocator);

        // Create a JSON array and adding conal_latitudes to the document
        rapidjson::Value direction(rapidjson::kArrayType);  // Create an empty array
        for (double element : aggregate.runtime_params.direction) {
            direction.PushBack(element, allocator);
        }
        document.AddMember("direction", direction, allocator);

        document.AddMember("dm_homogeneous", aggregate.runtime_params.dm_homogeneous, allocator);

        document.AddMember("scintillation_index_homo", aggregate.runtime_params.scintillation_index_homogeneous, allocator);

        document.AddMember("std_dm", aggregate.runtime_params.std_dm, allocator);

        document.AddMember("std_scintillation_index", aggregate.runtime_params.std_scintillation_index, allocator);

        // Convert the JSON document to a string (stringify it)
        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);  // Write the JSON document into the buffer

        // Now write the stringified JSON to a file
        std::ofstream ofs(filePath);
        if (ofs.is_open()) {
            ofs << buffer.GetString();  // Write the JSON string to the file
            ofs.close();
        } else {
            std::cerr << "Failed to open the file: " + filePath + " for writing. " << convert_relative_path(filePath) << std::endl;
        }
    }
};

#endif //PULSARDTPP_FILE_WRITER_HPP
