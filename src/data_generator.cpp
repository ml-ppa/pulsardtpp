//
// Created by Marcel Trattner on 29.01.24.
//

#include "data_generator.hpp"

#include <utility>



data_generator::data_generator(pulsar_animator anim) : animator(std::move(anim)) {};


std::pair<Eigen::MatrixXd, Eigen::MatrixXd>
data_generator::generate_data_spark_pattern(
        const std::vector<double>& rot_phases, std::vector<double> freq_channel, std::string tag, double signal_time_bin,
        const std::string& param_folder, const std::string& payload_folder, double antenna_sensitivity, double prob_nbrfi,
        double prob_bbrfi, double scale_direction_randomness, bool remove_drift_effect) {
    freq_channels = std::move(freq_channel);
    animator = pulsar_animator();
    // Payload is first created
    aggregated_payload aggregate(std::move(tag));

    std::vector<shared_ptr<spark_pattern_generator::spark>> spark_list = generate_random_spark_pattern(aggregate);
    animator.add_spark(spark_list);
    animator(0.);

    payload payload_obj = animator.generate_radio_packet_stream(rot_phases, aggregate, scale_direction_randomness);
    aggregate.add(aggregated_payload::LOAD_STATE::RAW, payload_obj);

    // Generate Payload to be propagated
    payload payload_to_be_propagated = generate_payload_at_acquisition_frequency(payload_obj);
    aggregate.add(aggregated_payload::LOAD_STATE::TO_BE_PROPAGATED, payload_to_be_propagated);

    interstellar_medium random_ism_obj = generate_random_ism_obj(aggregate);
    payload payload_propagated = random_ism_obj.propagate_through(payload_to_be_propagated, signal_time_bin, remove_drift_effect);

    antenna antenna_obj(payload_propagated, antenna_sensitivity, prob_nbrfi, prob_bbrfi);
    std::pair<Eigen::MatrixXd, Eigen::MatrixXd> total_noise_total_flux = antenna_obj.randomize_antenna_output();

    // Generate Payload detected
    payload payload_detected(payload_propagated.freqs);
    payload_detected.dataframe = total_noise_total_flux.first;
    payload_detected.assign_rot_phases(payload_propagated.rot_phases);
    payload_detected.desc = {antenna_obj.pulsar_count, antenna_obj.nbrfi_count, antenna_obj.bbrfi_count};
    aggregate.add(aggregated_payload::LOAD_STATE::DETECTED, payload_detected);

    payload payload_flux(payload_propagated.freqs);
    payload_flux.dataframe = total_noise_total_flux.second;
    payload_flux.assign_rot_phases(payload_propagated.rot_phases);
    payload_flux.desc = {antenna_obj.pulsar_count, antenna_obj.nbrfi_count, antenna_obj.bbrfi_count};
    aggregate.add(aggregated_payload::LOAD_STATE::FLUX, payload_flux);

    file_writer::write_testrun(aggregate, param_folder, payload_folder);

    return total_noise_total_flux;
}

std::vector<shared_ptr<spark_pattern_generator::spark>>
data_generator::generate_random_spark_pattern(
        aggregated_payload& aggregate, int num_cones, double avg_spark_dimension, float avg_spark_per_cone_length,
        double avg_spark_pattern_center_tilt, double avg_drift_vel){
    // Generate spark dimension
    double spark_dimension = std::max(0.0, calc::normal_distribution(avg_spark_dimension, avg_spark_dimension / 4));
    double spark_dimension_radian = spark_dimension * std::numbers::pi / 180.0;

    // Generate sparks per cone length
    int spark_per_cone_length = std::max(1, static_cast<int>(calc::normal_distribution(avg_spark_per_cone_length, avg_spark_per_cone_length / 4.)));

    // Generate conal latitudes
    std::vector<double> conal_latitudes(num_cones);
    for (int i = 0; i < num_cones; ++i) {
        conal_latitudes[i] = 0.01 + i * spark_dimension * 2;
    }

    // Generate conal circumferences
    std::vector<double> conal_circumference(num_cones);
    for (int i = 0; i < num_cones; ++i) {
        conal_circumference[i] = 2 * std::numbers::pi * std::cos(conal_latitudes[i] * std::numbers::pi / 180.0);
    }

    // Generate number of sparks
    std::vector<int> num_sparks(num_cones);
    for (int i = 0; i < num_cones; ++i) {
        num_sparks[i] = static_cast<int>(std::ceil(conal_circumference[i] * spark_per_cone_length));
    }

    // Generate spark pattern center tilt
    Eigen::Vector2d spark_rotation_axis_polar_att0 = {avg_spark_pattern_center_tilt, (calc::uniform_real_distribution()*360) };

    // Generate drift velocity
    double drift_vel = calc::normal_distribution(avg_drift_vel, avg_drift_vel / 4);

    // Generate spark mid frequencies and flux powers
    std::vector<double> spark_mid_freqs(num_cones);
    std::vector<double> spark_flux_powers(num_cones);
    for (int i = 0; i < num_cones; ++i) {
        spark_mid_freqs[i] = 0.50 * (i + 1);
        spark_flux_powers[i] = 10. * 1;
    }

    std::vector<shared_ptr<spark_pattern_generator::spark>> list = spark_pattern_generator::create_patterned_spark_pattern(
            num_sparks,
            conal_latitudes,
            spark_rotation_axis_polar_att0,
            drift_vel,
            spark_dimension_radian,
            spark_mid_freqs,
            spark_flux_powers);

    animator.spark_spectrum_model_list.reserve(list.size());
    for (size_t i = 0; i < list.size(); ++i) {
        animator.spark_spectrum_model_list.emplace_back(spark_pattern_generator::spark_spectral_model());
    }

    // Add spark list for runtime arguments
    aggregate.runtime_params.num_sparks = num_sparks;
    aggregate.runtime_params.conal_latitudes = conal_latitudes;
    aggregate.runtime_params.spark_rotation_axis_polar_att0 = spark_rotation_axis_polar_att0;
    aggregate.runtime_params.drift_vel = drift_vel;
    aggregate.runtime_params.spark_dimension = spark_dimension_radian;
    aggregate.runtime_params.spark_mid_freqs = spark_mid_freqs;
    aggregate.runtime_params.spark_flux_powers = spark_flux_powers;

    return list;
}

payload
data_generator::generate_payload_at_acquisition_frequency(const payload& payload_raw) const {
    const auto& spark_spectrum_models = animator.spark_spectrum_model_list;
    const auto& spark_mid_freq = payload_raw.freqs;

    Eigen::MatrixXd relative_flux_mat(freq_channels.size(), spark_mid_freq.size());

    for (size_t i = 0; i < spark_mid_freq.size(); ++i) {
        Eigen::VectorXd channel_flux = spark_spectrum_models[i](spark_mid_freq[i], freq_channels);
        relative_flux_mat.col(i) = channel_flux;
    }

    Eigen::MatrixXd dataframe = relative_flux_mat * payload_raw.dataframe.transpose();

    payload payload_to_be_propagated(freq_channels);
    payload_to_be_propagated.dataframe = dataframe.transpose();

    payload_to_be_propagated.assign_rot_phases(payload_raw.rot_phases);
    return payload_to_be_propagated;
}

interstellar_medium
data_generator::generate_random_ism_obj(aggregated_payload& aggregate) {
    interstellar_medium avg_ism_obj = interstellar_medium();
    avg_ism_obj.randomize_props();

    // write ism obj to runtime
    aggregate.runtime_params.dm_homogeneous = avg_ism_obj.dm_homogeneous;
    aggregate.runtime_params.scintillation_index_homogeneous = avg_ism_obj.scintillation_index_homogeneous;
    aggregate.runtime_params.std_dm = avg_ism_obj.std_dm;
    aggregate.runtime_params.std_scintillation_index = avg_ism_obj.std_scintillation_index;

    return avg_ism_obj;
}