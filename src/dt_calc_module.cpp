#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <Eigen/Dense>
#include <vector>

#include "common/reference_frame_operation.hpp"
#include "common/calculation.hpp"

namespace py = pybind11;


PYBIND11_MODULE(dt_calc_module, m) {
    m.def("polar2cart", &reference_frame_operation::polar2cart, "Convert Vector from polar to cartesian.", 
        py::arg("r"), py::arg("theta"), py::arg("phi"));

    m.def("rotate_reference_frame", &reference_frame_operation::rotate_reference_frame, "Process a vector of Eigen::Vector3d with a shift vector and scale factor",
        py::arg("frame_0_basis"), py::arg("axis_of_rotation"), py::arg("rotation_angle"));

    m.def("interp", &calculation::interp, "One-dimensional linear interpolation for monotonically increasing sample points.",
        py::arg("x"), py::arg("xp"), py::arg("fp"), py::arg("left") = NAN, 
        py::arg("period") = NAN, py::arg("right") = NAN);
}
