//
// Created by Marcel Trattner on 25.07.24.
//

#include "spark_pattern_generator.hpp"

#include <utility>

spark_pattern_generator::spark::spark(
        int spark_id, double spark_dimension, Eigen::Vector2d spark_center,
        Eigen::Vector2d spark_rotation_axis_polar_att0, double drift_vel,  double drift_phase,
        double mid_freq, double flux_power
        ): spark_id(spark_id), _spark_dimension(spark_dimension), _spark_center(std::move(spark_center)),
        _spark_rotation_axis_polar_att0(std::move(spark_rotation_axis_polar_att0)), _drift_vel(drift_vel),
        _drift_phase(drift_phase), _mid_freq(mid_freq), _flux_power(flux_power){
};

double
spark_pattern_generator::spark::apply_physics_model(
        const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis,
        double pulsar_rot_phase,
        const Eigen::Vector3d& position)
{
    Eigen::Vector3d spark_center_cartesian_att = calculate_spark_center_att(
            pulsar_rot_frame_basis, pulsar_rot_phase);

    double position_mag = position.norm();
    Eigen::Vector3d normalized_position = position / position_mag;

    double radian_distance = std::acos(normalized_position.dot(spark_center_cartesian_att));

    double amplitude;
    if (radian_distance >= SPARK_RADIUS_THRESH_FACTOR * _spark_dimension) {
        amplitude = 0.0;
    } else {
        amplitude = _flux_power * std::exp(
                -(pow(radian_distance, 2)) / (2 * pow(_spark_dimension, 2)));
    }

    return amplitude;
}

Eigen::Vector3d
spark_pattern_generator::spark::calculate_spark_center_att(
        const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis,
        double pulsar_rot_phase)
{
    std::vector<Eigen::Vector3d> mag_frame_basis = spark::calculate_magframe_basis_att(
            pulsar_rot_frame_basis, pulsar_rot_phase);

    double rotation_angle = _drift_vel * pulsar_rot_phase / 180.0 * std::numbers::pi
                            + _drift_phase / 180.0 * std::numbers::pi;
    Eigen::AngleAxisd rotation_vector(rotation_angle, mag_frame_basis[2].normalized());
    Eigen::Quaterniond quaternion_rotator(rotation_vector);

    Eigen::Vector3d spark_center_cartesian_att0_in_mag_frame = polar2cart(
            1,
            _spark_center[0] / 180.0 * std::numbers::pi,
            _spark_center[1] / 180.0 * std::numbers::pi
    );

    Eigen::Vector3d spark_center_cartesian_att0 = Eigen::Vector3d::Zero();
    for (int x = 0; x < 3; ++x) {
        spark_center_cartesian_att0 += spark_center_cartesian_att0_in_mag_frame[x] * mag_frame_basis[x];
    }

    Eigen::Vector3d spark_center_cartesian_att = quaternion_rotator * spark_center_cartesian_att0;
    return spark_center_cartesian_att;
}

std::vector<Eigen::Vector3d>
spark_pattern_generator::spark::calculate_magframe_basis_att(
        const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis,
        double pulsar_rot_phase)
{
    const Eigen::Vector3d& rot_frame_z_axis = pulsar_rot_frame_basis[2];
    auto [rot_frame_x_axis, rot_frame_y_axis] = find_perpendicular_basis_vectors(rot_frame_z_axis);

    std::vector<Eigen::Vector3d> mag_frame_basis_att0 = rotate_reference_frame(
            {rot_frame_x_axis, rot_frame_y_axis, rot_frame_z_axis},
            rot_frame_y_axis,
            _spark_rotation_axis_polar_att0[0] / 180.0 * std::numbers::pi
    );

    mag_frame_basis_att0 = rotate_reference_frame(
            mag_frame_basis_att0,
            rot_frame_z_axis,
            _spark_rotation_axis_polar_att0[1] / 180.0 * std::numbers::pi
    );

    std::vector<Eigen::Vector3d> mag_frame_basis = rotate_reference_frame(
            mag_frame_basis_att0,
            rot_frame_z_axis,
            pulsar_rot_phase * std::numbers::pi / 180.0
    );

    return mag_frame_basis;
}

Eigen::Vector3d
spark_pattern_generator::spark::ortho_vector_parametric(double a, double b, double c, double parameter) {
    double cos_param = std::cos(parameter);
    double sin_param = std::sin(parameter);
    double sqrt_ab = std::sqrt(a * a + b * b);

    Eigen::Vector3d ortho_vector = {
            -b * cos_param - a * c / sqrt_ab * sin_param,
            a * cos_param - b * c / sqrt_ab * sin_param,
            sqrt_ab * sin_param
    };

    return ortho_vector;
}

std::pair<Eigen::Vector3d, Eigen::Vector3d>
spark_pattern_generator::spark::find_perpendicular_basis_vectors(const Eigen::Vector3d &vector) {
    double a = vector[0];
    double b = vector[1];
    double c = vector[2];

    Eigen::Vector3d ortho_vector_x{};
    Eigen::Vector3d ortho_vector_y{};

    if (a == 0 && b == 0) {
        ortho_vector_x = {1, 0, 0};
        ortho_vector_y = {0, 1, 0};
    } else {
        ortho_vector_x = ortho_vector_parametric(a, b, c, 0);
        ortho_vector_y = ortho_vector_parametric(a, b, c, std::numbers::pi / 2);
    }

    return std::make_pair(ortho_vector_x, ortho_vector_y);
}

vector<shared_ptr<spark_pattern_generator::spark>>
spark_pattern_generator::create_patterned_spark_pattern(const vector<int> &num_sparks,
                                                        const vector<double> &conal_latitudes,
                                                        const Eigen::Vector2d &spark_rotation_axis_polar_att0 ,
                                                        const double drift_vel, const double spark_dimension,
                                                        const vector<double> &spark_mid_freqs,
                                                        const vector<double> &spark_flux_powers) {

    std::vector<std::shared_ptr<spark_pattern_generator::spark>> spark_list;
    int spark_id_noter = 0;

    for (size_t id = 0; id < conal_latitudes.size(); ++id) {
        int num_spark = num_sparks[id];
        double spark_freq = !spark_mid_freqs.empty() ? (spark_mid_freqs)[id] : 125.0;
        double spark_flux_power = !spark_flux_powers.empty() ? (spark_flux_powers)[id] : 1.0;

        std::vector<int> spark_ids(num_spark);
        std::iota(spark_ids.begin(), spark_ids.end(), spark_id_noter);

        std::vector<double> phases(num_spark);
        for (int i = 0; i < num_spark; ++i) {
            phases[i] = i * 360.0 / num_spark;
        }

        for (size_t i = 0; i < spark_ids.size(); ++i) {
            spark_list.push_back(std::make_shared<spark_pattern_generator::spark>(
                    spark_ids[i], spark_dimension, Eigen::Vector2d{conal_latitudes[id], 0.0},
                    spark_rotation_axis_polar_att0, drift_vel, phases[i], spark_freq, spark_flux_power));
        }

        spark_id_noter += num_spark;
    }

    return spark_list;
}

Eigen::VectorXd
spark_pattern_generator::spark_spectral_model::operator()(const double center_freq, const std::vector<double> frequencies) const {
    Eigen::VectorXd relative_amplitude = Eigen::VectorXd::Map(frequencies.data(), frequencies.size());

    switch (this->model) {
        case 0:
            relative_amplitude = (relative_amplitude / center_freq);// spectral_index);
            relative_amplitude = relative_amplitude.array().pow(spectral_index);
            relative_amplitude = b * relative_amplitude;
            break;
        case 1:
            break;
        default:
            throw std::invalid_argument("Spark Spectral Model identifier has not been set");
    }
    return relative_amplitude;
}
