//
// Created by Marcel Trattner on 29.02.24.
//

#ifndef PULSARDTPP_INTERSTELLAR_MEDIUM_HPP
#define PULSARDTPP_INTERSTELLAR_MEDIUM_HPP

#include <vector>
#include "../common/calculation.hpp"
#include "../io_management/payload.hpp"

namespace calc = calculation;

class interstellar_medium {

public:
    double std_dm;
    double std_scintillation_index;
    double scintillation_index_homogeneous;
    double dm_homogeneous;

    interstellar_medium(double std_dm = 0.02, double std_scintillation_index = 100, double scintillation_index_homogeneous = 100., double dm_homogeneous = 2.643);

    double calc_disperse_phase_shift(double freq, double freq_ref = 1., double signal_time_bin = 1.) const;

    void randomize_props();

    payload propagate_through(payload payload, double signal_time_bin, bool average_over_period=true);
};


#endif //PULSARDTPP_INTERSTELLAR_MEDIUM_HPP
