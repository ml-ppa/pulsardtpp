import numpy as np

from pulsar_simulation._m_pulsar_animator import update_pulsar_state_vectors
from .information_packet_formats import Payload
from .spark_pattern_generator import (
    SparkPatternGenerator,
    SparkSpectralModel,
)

class PulsarAnimator:
    """This class animates a pulsar rotator as viewed from Earth. The frame of reference is the Earth's celestial coordinate system"""

    def __init__(self, rot_axis: list, mag_axis_tilt: float, period: float = 33):
        """Initializes a pulsar animator with pulsar state vectors at time = 0

        Args:
            rot_axis (list): 3d list representing the rotation axis of pulsar in the earths frame of reference (except translation)
            mag_axis_tilt (float): representing magnetic axis tilt away from rot axis in degrees
            period (float): time in ms for one complete rotation of the pulsar
        """
        self.__rot_axis = rot_axis
        self.__mag_axis_tilt = mag_axis_tilt
        self.period = period
        
        self.mag_frame_basis_att0 = None
        
        self.rot_frame_basis = None
        self.mag_frame_basis = None
        self.spark_genlist = []
        self.spark_spectrum_model_list = []

    def get_rot_axis(self):
        return self.__rot_axis
    
    def get_mag_axis_tilt(self):
        return self.__mag_axis_tilt

    def __call__(self, rot_phase: float):
        """This function needs to be called to return the magnetic frame basis vectors at a given rotational phase

        Args:
            rot_phase (float): rotation phase in degrees

        Returns:
            mag_frame_basis (list[list]): returns magnetic frame basis vectors
        """
        tuple = update_pulsar_state_vectors(self.__rot_axis, self.__mag_axis_tilt, rot_phase)
        self.mag_frame_basis_att0 = tuple[0]
        self.rot_frame_basis = tuple[1]
        self.mag_frame_basis = tuple[2]
        return tuple[2]

    def generate_radio_packet(self, rot_phase: float, direction: list[float]):
        """This method generates a tuple lists of radio flux values and the corresponding center frequencies of the sparks

        Args:
            rot_phase (float): The rotation phase of the pulsar in degrees
            direction (list[float]): a vector specifying the line of sight towards Earth

        Returns:
            tuple[list,list]: radio packet as tuple containing list of flux values and corresponding center frequencies of the sparks
        """
        
        pulsar_rot_frame_basis = self.rot_frame_basis
        spark_patterns = self.spark_genlist
        radio_packet = self.calculate_radio_packet_at_direction(
            spark_patterns=spark_patterns,
            direction=direction,
            pulsar_rot_frame_basis=pulsar_rot_frame_basis,
            rot_phase=rot_phase,
        )
        return radio_packet

    @staticmethod
    def calculate_radio_packet_at_direction(
        spark_patterns: list[SparkPatternGenerator],
        
        direction: list[float],
        pulsar_rot_frame_basis: list[list],
        rot_phase: float,
    ):
        """This method calculates a radio packet at a particular direction

        Args:
            spark_patterns (list[SparkPatternGenerator]): List of radio hot spots or sparks
            direction (list[float]): vector representing the direction at which the packet is emitted
            pulsar_rot_frame_basis (list[list]): pulsars static frame basis vectors where the pulsar's rotation axis is alligned to the z axis
            rot_phase (float): rotation phase in degrees

        Returns:
            (list,list): radio packet as tuple containing list of flux values and corresponding center frequencies of the sparks
        """
        mid_freq_list = np.array([spark.get_spark_freq() for spark in spark_patterns])
        flux_from_sparks_list = np.array(
            [
                spark(
                    position=direction,
                    pulsar_rot_frame_basis=pulsar_rot_frame_basis,
                    pulsar_rot_phase=rot_phase,
                )
                for spark in spark_patterns
            ]
        )
        return flux_from_sparks_list.tolist(), mid_freq_list.tolist()

    def generate_radio_packet_stream(
        self, rot_phases: list[float], direction: list[float]
    ):
        """This method calculates radio packets at a particular direction for the list of rotation phases

        Args:
            rot_phases (list[float]): list of rotation phases in degrees
            direction (list[float]): vector representing the direction at which the packet is emitted


        Returns:
            Payload: payload object representing the radi data stream
        """
        rot_phases = [rf * 1.0 for rf in rot_phases]
        freq_list = [spark.get_spark_freq() for spark in self.spark_genlist]
        payload_obj = Payload(freqs=freq_list)
        _ = [
            payload_obj.add_flux(
                radio_packet=self.generate_radio_packet(
                    rot_phase=x, direction=direction
                )
            )
            for x in rot_phases
        ]

        payload_obj.assign_rot_phases(rot_phases=rot_phases)
        return payload_obj