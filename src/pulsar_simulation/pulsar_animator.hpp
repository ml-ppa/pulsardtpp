//
// Created by Marcel Trattner on 01.08.24.
//

#ifndef PULSARDTPP_PULSAR_ANIMATOR_HPP
#define PULSARDTPP_PULSAR_ANIMATOR_HPP

#include <vector>
#include <numbers>
#include <memory>
#include "../io_management/payload.hpp"
#include "spark_pattern_generator.hpp"

namespace calc = calculation;

class pulsar_animator {
public:
    Eigen::Vector3d __rot_axis;
    double __mag_axis_tilt;

    std::vector<Eigen::Vector3d> mag_frame_basis_att0;
    std::vector<Eigen::Vector3d> rot_frame_basis;
    std::vector<Eigen::Vector3d> mag_frame_basis;
    std::vector<shared_ptr<spark_pattern_generator::spark>> spark_genlist;

    std::vector<spark_pattern_generator::spark_spectral_model> spark_spectrum_model_list;

    pulsar_animator(const Eigen::Vector3d& rot_axis = {0,0,1}, double mag_axis_tilt = 45.);

    void add_spark(std::vector<shared_ptr<spark_pattern_generator::spark>> spark_gen);

    std::vector<Eigen::Vector3d> operator()(double rot_phase);

    void update_pulsar_state_vectors(double rot_phase);

    static std::pair<Eigen::Vector3d, Eigen::Vector3d> find_perpendicular_basis_vectors(const Eigen::Vector3d& vector);

    std::pair<std::vector<double>, std::vector<double>> generate_radio_packet(double rot_phase, const Eigen::Vector3d& direction);

    static std::pair<std::vector<double>, std::vector<double>> calculate_radio_packet_at_direction(
            std::vector<shared_ptr<spark_pattern_generator::spark>>& spark_patterns,
            const Eigen::Vector3d& direction,
            const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis,
            double rot_phase);

    payload generate_radio_packet_stream(const std::vector<double>& rot_phases, aggregated_payload& aggregate, const double& scale_direction_randomness=0.5);

private:
    std::vector<Eigen::Vector3d> rotate_reference_frame(
            const std::vector<Eigen::Vector3d>& frame_0_basis,
            const Eigen::Vector3d& axis_of_rotation,
            double rotation_angle);
};


#endif //PULSARDTPP_PULSAR_ANIMATOR_HPP
