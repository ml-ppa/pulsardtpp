//
// Created by Marcel Trattner on 15.03.24.
//

#ifndef PULSARDTPP_ANTENNA_HPP
#define PULSARDTPP_ANTENNA_HPP

#include <cmath>
#include <random>
#include <Eigen/Dense>
#include "../io_management/payload.hpp"
#include "../common/calculation.hpp"

namespace calc = calculation;

class antenna {
private:
    std::vector<double> freq_channels;
    std::vector<double> rot_phases;
    Eigen::MatrixXd dataframe;

    double sensitivity;
    double prob_nbrfi;
    double prob_bbrfi;

public:

    int nbrfi_count = 0;
    int bbrfi_count = 0;
    int pulsar_count = 0;


    antenna(payload& cargo, double sensitivity=0.1, double prob_nbrfi=1., double prob_bbrfi=1.);

    Eigen::MatrixXd calculate_antenna_output_voltage_matrix(const Eigen::MatrixXd& received_flux);

    Eigen::MatrixXd gen_noise_NBRFI(double effected_freq_channel, double nbrfi_amplitude = 1.0, double spread_in_channels = 0.5);

    Eigen::MatrixXd gen_noise_BBRFI(double effected_rot_phase, double bbrfi_amplitude = 1.0, double spread_in_phase = 10.0);

    double antenna_temperature(double received_flux);

    Eigen::MatrixXd gen_terresterial_noise(double effected_freq_channel_by_nbrfi, double nbrfi_bandwidth, double effected_rot_phase_by_bbrfi,
                                           double bbrfi_duration, const std::vector<double>& noise_amplitude_range);

    std::pair<Eigen::MatrixXd, Eigen::MatrixXd> randomize_antenna_output();

    void is_pulsar_present();
};


#endif //PULSARDTPP_ANTENNA_HPP
