//
// Created by Marcel Trattner on 29.02.24.
//

#include "interstellar_medium.hpp"

interstellar_medium::interstellar_medium(double std_dm, double std_scintillation_index, double scintillation_index_homogeneous, double dm_homogeneous) :
        std_dm{std_dm}, std_scintillation_index{std_scintillation_index}, scintillation_index_homogeneous{scintillation_index_homogeneous} , dm_homogeneous{dm_homogeneous} {

}

double
interstellar_medium::calc_disperse_phase_shift(double freq, double freq_ref, double signal_time_bin) const {
    return (4.15 * dm_homogeneous / 1 * ((1 / pow(freq, 2)) - (1 / pow(freq_ref, 2))) / signal_time_bin );
}


void interstellar_medium::randomize_props() {
    dm_homogeneous = std::max(0., calc::normal_distribution(dm_homogeneous, std_dm));
    scintillation_index_homogeneous = std::max(0., calc::normal_distribution(scintillation_index_homogeneous, std_scintillation_index));
}

payload interstellar_medium::propagate_through(payload cargo, double signal_time_bin, bool average_over_period) {
    Eigen::VectorXd rot_phases = Eigen::Map<Eigen::VectorXd>(cargo.rot_phases.data(), cargo.rot_phases.size());
    std::vector<double> freq_channels = cargo.freqs;
    Eigen::MatrixXd flux_freq_channels = cargo.dataframe;

    double max_freq = *std::max_element(freq_channels.begin(), freq_channels.end());

    Eigen::VectorXd rot_phase_shifts(freq_channels.size());
    for (size_t i = 0; i < freq_channels.size(); ++i) {
        rot_phase_shifts[i] = calc_disperse_phase_shift(freq_channels[i], max_freq, signal_time_bin);
    }

    // Transpose the flux_freq_channels matrix
    Eigen::MatrixXd flux_freq_channels_transposed = flux_freq_channels.transpose();

    // Initialize the dispersed_flux matrix with the same size as flux_freq_channels
    Eigen::MatrixXd dispersed_flux(freq_channels.size(), rot_phases.size());

    for (size_t id = 0; id < rot_phase_shifts.size(); ++id) {
        Eigen::VectorXd interp_flux(freq_channels.size());
        Eigen::VectorXd shifted_rot_phases = rot_phases.array() + rot_phase_shifts[id];
        if (average_over_period) {
            interp_flux = calculation::interp(rot_phases, shifted_rot_phases, flux_freq_channels_transposed.row(id), 0, 360);
        } else {
            interp_flux = calculation::interp(rot_phases, shifted_rot_phases, flux_freq_channels_transposed.row(id), 0);
        }

        dispersed_flux.row(id) = interp_flux.transpose();
    }

    // Flip freq_channels
    std::reverse(freq_channels.begin(), freq_channels.end());

    // Flip the columns of dispersed_flux to match the reversed freq_channels
    dispersed_flux = dispersed_flux.colwise().reverse().eval();

    // Transpose the dispersed_flux matrix
    Eigen::MatrixXd dispersed_flux_transposed = dispersed_flux.transpose();

    // Create a new payload and assign the processed data
    payload payload_propagated(freq_channels);
    payload_propagated.dataframe = dispersed_flux_transposed;
    payload_propagated.assign_rot_phases(cargo.rot_phases);

    return payload_propagated;
}