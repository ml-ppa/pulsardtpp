import numpy as np
import time
import ray
from tqdm import tqdm
import matplotlib.pyplot as plt

from .pulsar_animator import PulsarAnimator
from .remote_functions import run_gen_data_method
from pulsar_simulation._m_data_generation_module import generate_single


class GenData:
    def __init__(self,
                 pulsar_animator:PulsarAnimator,
                 ): #cannot be in more than 1 place
        self.pulsar_animator = pulsar_animator
        self.param_folder = []
        
        
    def __call__(self,
                 freq_channels:list[float]=None,
                 rot_phases:list[float]=None,
                 tag:str='_test_0',
                 signal_time_bin:float=33.0/360,
                 param_folder:str='../params/runtime/',
                 payload_folder:str='../params/payloads/',
                 antenna_sensitivity:float = 0.5,
                 prob_nbrfi:float=0.5,
                 prob_bbrfi:float=0.5,
                 scale_direction_randomness:float=0.5,
                 remove_drift_effect:bool=True
                 ):

        return generate_single(self.pulsar_animator.get_rot_axis(), self.pulsar_animator.get_mag_axis_tilt(), 
                              rot_phases, freq_channels, tag, signal_time_bin, param_folder, payload_folder,
                              antenna_sensitivity, prob_nbrfi, prob_bbrfi, scale_direction_randomness, remove_drift_effect)
    
    def plot(self,
                 freq_channels:list[float]=None,
                 rot_phases:list[float]=None,
                 tag:str='_test_0',
                 signal_time_bin:float=33.0/360,
                 param_folder:str='../params/runtime/',
                 payload_folder:str='../params/payloads/',
                 antenna_sensitivity:float = 0.5,
                 prob_nbrfi:float=0.5,
                 prob_bbrfi:float=0.5,
                 scale_direction_randomness:float=0.5,
                 remove_drift_effect:bool = True
                 ):
        total_noisy_output,total_flux = generate_single(self.pulsar_animator.get_rot_axis(), self.pulsar_animator.get_mag_axis_tilt(), 
                              rot_phases, freq_channels, tag, signal_time_bin, param_folder, payload_folder,
                              antenna_sensitivity, prob_nbrfi, prob_bbrfi, scale_direction_randomness, remove_drift_effect)
        fig1, ax = plt.subplots(1, 2, figsize=(10, 5))
        # dist_output = ConnectedComponents.skeletonize_image(dispersed_freq_time_segmented=dispersed_freq_time_segmented)
        ax[0].imshow(total_noisy_output.T)
        ax[0].set_xlabel("phase")
        ax[0].set_ylabel("freq channel")
        ax[0].grid("True")
        ax[0].set_aspect("auto")
        #ax[0].invert_yaxis()       
        ax[1].imshow(total_flux.T)
        ax[1].set_xlabel("phase")
        ax[1].set_ylabel("freq channel")
        ax[1].grid("True")
        ax[1].set_aspect("auto")

    
def run_gen_data_in_parallel(gendata_obj:GenData,
                             tag_list:list[str],
                             rot_phases=np.arange(0,720*1,1).tolist(),
                             freq_channels=np.arange(0.5,1.6,0.001),
                             antenna_sensitivity:float = 0.5,
                             prob_nbrfi:float=0.5,
                             prob_bbrfi:float=0.5,
                             scale_direction_randomness:float=0.5,
                             num_cpus:int=10,
                             return_execution_time:bool=True,
                             reinit_ray:bool=True,
                             remove_drift_effect:bool =True,
                             param_folder='../params/runtime/',
                             payload_folder:str='../params/payloads/'):
    
    if ray.is_initialized() and reinit_ray:
        ray.shutdown()
        ray.init(num_cpus=num_cpus)
    

    freq_channels_shared = ray.put(freq_channels)
    rot_phases_shared = ray.put(rot_phases)
    payload_folder_shared = ray.put(payload_folder) 
    param_folder_shared = ray.put(param_folder)
    antenna_sensitivity_shared = ray.put(antenna_sensitivity)
    prob_nbrfi_shared=ray.put(prob_nbrfi)
    prob_bbrfi_shared=ray.put(prob_bbrfi)
    scale_direction_randomness_shared:float=ray.put(scale_direction_randomness)
    remove_drift_effect_shared = ray.put(remove_drift_effect)
    
    if return_execution_time:strt_time = time.time()
    ray.get([run_gen_data_method.options(scheduling_strategy="SPREAD").remote(func=gendata_obj,tag=tg,
                                                 rot_phases=rot_phases_shared,
                                                 freq_channels=freq_channels_shared,
                                                 antenna_sensitivity = antenna_sensitivity_shared,
                                                 prob_nbrfi = prob_nbrfi_shared,
                                                 prob_bbrfi = prob_bbrfi_shared,
                                                 scale_direction_randomness = scale_direction_randomness_shared,
                                                 remove_drift_effect = remove_drift_effect_shared,
                                                 payload_folder=payload_folder_shared,
                                                 param_folder=param_folder_shared) for tg in tag_list])

    if return_execution_time: return time.time()-strt_time


def generate_randomized_data_payloads(
        gendata_obj:GenData,
        tag:str,
        num_payloads:int,
        rot_phases=np.arange(0,720*1,1).tolist(),
        freq_channels=np.arange(0.5,1.6,0.001),
        antenna_sensitivity:float = 0.5,
        prob_nbrfi:float=0.5,
        prob_bbrfi:float=0.5,
        scale_direction_randomness:float=0.5,
        remove_drift_effect:bool =True,
        num_cpus:int=10,               
        param_folder='../params/runtime/',
        payload_folder:str='../params/payloads/',
        reinit_ray:bool=True,
        tag_index_offset:int=0
        ):
    
    if ray.is_initialized() and reinit_ray:
        ray.shutdown()
        ray.init(num_cpus=num_cpus)

    tags = [tag+str(x+tag_index_offset) for x in range(num_payloads)] 
    distributed_tag_list = [tags[x*num_cpus:(x+1)*num_cpus] for x in range(np.ceil(len(tags)/num_cpus).astype(int))]
    for tag_list in tqdm(distributed_tag_list):
        run_gen_data_in_parallel(gendata_obj=gendata_obj,
                         rot_phases=rot_phases,
                         antenna_sensitivity=antenna_sensitivity,
                         freq_channels=freq_channels,
                         tag_list=tag_list,
                         param_folder=param_folder,
                         payload_folder=payload_folder,
                         reinit_ray=False,
                         return_execution_time=False,
                         prob_bbrfi=prob_bbrfi,
                         prob_nbrfi=prob_nbrfi,
                         scale_direction_randomness=scale_direction_randomness,
                         remove_drift_effect=remove_drift_effect)

def generate_example_payloads_for_training( tag:str='train_v0_',
                                            num_payloads:int=10,
                                            rot_phases=np.arange(0,360*2,5).tolist(),
                                            freq_channels=np.arange(0.25,.85,0.001),
                                            antenna_sensitivity:float = 0.5,
                                            prob_nbrfi:float=0.5,
                                            prob_bbrfi:float=0.5,
                                            scale_direction_randomness:float=0.5,
                                            remove_drift_effect:bool =True,
                                            num_cpus:int=10,               
                                            param_folder='./params/runtime/',
                                            payload_folder:str='./params/payloads/',
                                            reinit_ray:bool=True,
                                            tag_index_offset:int=0,
                                            plot_a_example:bool=True,                                            
                                            ):
    pulsar_animator = PulsarAnimator(rot_axis=[0,0,1],mag_axis_tilt=45)
    pulsar_animator(rot_phase=0)
    gendata_obj = GenData(pulsar_animator=pulsar_animator)
    rot_phases =  rot_phases
    freq_channels = freq_channels
    if plot_a_example:
        gendata_obj.plot(tag=tag,
                rot_phases=rot_phases,
                freq_channels=freq_channels,
                param_folder=param_folder,
                payload_folder=payload_folder,
                antenna_sensitivity=antenna_sensitivity,
                prob_bbrfi=prob_bbrfi,
                prob_nbrfi=prob_nbrfi,
                scale_direction_randomness=scale_direction_randomness,
                remove_drift_effect=remove_drift_effect
                )
    generate_randomized_data_payloads(gendata_obj=gendata_obj,
                         rot_phases=rot_phases,
                         freq_channels=freq_channels,
                         tag=tag,
                         num_payloads=num_payloads,
                         param_folder=param_folder,payload_folder=payload_folder,
                         prob_bbrfi=prob_bbrfi,
                         prob_nbrfi=prob_nbrfi,
                         antenna_sensitivity=antenna_sensitivity,
                         tag_index_offset=tag_index_offset,
                         num_cpus=num_cpus,
                         reinit_ray=reinit_ray)