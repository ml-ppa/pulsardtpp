import os

num_cpus = os.cpu_count()
numTaskpCPU = 1
import ray


@ray.remote(num_cpus=1)
def run_gen_data_method(func, **kwargs):
    func(**kwargs)