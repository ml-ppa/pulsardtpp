//
// Created by Marcel Trattner on 15.03.24.
//

#include "antenna.hpp"

#include <utility>

antenna::antenna(payload& cargo, double sensitivity, double prob_nbrfi, double prob_bbrfi) : sensitivity{sensitivity}, prob_nbrfi{prob_nbrfi}, prob_bbrfi{prob_bbrfi} {
    freq_channels = cargo.freqs;
    rot_phases = cargo.rot_phases;
    dataframe = cargo.dataframe;
}

Eigen::MatrixXd antenna::gen_noise_NBRFI(double effected_freq_channel, double nbrfi_amplitude, double spread_in_channels) {
    // Randomly select an effected phase
    double effected_phase = rot_phases[calc::uniform_real_distribution(0, rot_phases.size() - 1)];

    // Construct frequency and phase matrices using Eigen
    Eigen::MatrixXd freq_channel_mat(freq_channels.size(), rot_phases.size());
    Eigen::MatrixXd phase_mat(freq_channels.size(), rot_phases.size());

    for (size_t i = 0; i < freq_channels.size(); ++i) {
        freq_channel_mat.row(i).setConstant(freq_channels[i]);
    }

    for (size_t j = 0; j < rot_phases.size(); ++j) {
        phase_mat.col(j).setConstant(rot_phases[j]);
    }

    Eigen::MatrixXd nbrfi_noise = nbrfi_amplitude
        * (-((-effected_freq_channel + freq_channel_mat.array()).array().square())
         / (2 * std::pow(spread_in_channels, 2))).exp()
        * (-((-effected_phase + phase_mat.array()).array().square())
         / (2 * std::pow(720.0, 2))).exp();

    return nbrfi_noise;
}

Eigen::MatrixXd antenna::gen_noise_BBRFI(double effected_rot_phase, double bbrfi_amplitude, double spread_in_phase) {
    // Randomly select a frequency channel
    double effected_freq_channel = freq_channels[calc::uniform_real_distribution(0, freq_channels.size() - 1)];

    // Construct frequency and phase matrices using Eigen
    Eigen::MatrixXd freq_channel_mat(freq_channels.size(), rot_phases.size());
    Eigen::MatrixXd phase_mat(freq_channels.size(), rot_phases.size());

    for (size_t i = 0; i < freq_channels.size(); ++i) {
        freq_channel_mat.row(i).setConstant(freq_channels[i]);
    }

    for (size_t j = 0; j < rot_phases.size(); ++j) {
        phase_mat.col(j).setConstant(rot_phases[j]);
    }

    Eigen::MatrixXd bbrfi_noise = bbrfi_amplitude
        * (-((-effected_freq_channel + freq_channel_mat.array()).array().square())
         / (2 * std::pow(10, 2))).exp()
        * (-((-effected_rot_phase + phase_mat.array()).array().square())
         / (2 * std::pow(spread_in_phase, 2))).exp();

    return bbrfi_noise;
}

Eigen::MatrixXd antenna::calculate_antenna_output_voltage_matrix(const Eigen::MatrixXd& T_A) {
    const double T_S = 1.0;
    const double bandwidth = 1.0;
    const double acquisitiontime = 1.0;

    Eigen::MatrixXd voltage_mean = T_A.array() + T_S;
    Eigen::MatrixXd voltage_std = voltage_mean / std::sqrt(bandwidth * acquisitiontime);

    // Prepare the output matrix
    Eigen::MatrixXd voltage_output(T_A.rows(), T_A.cols());

    // Calculate the voltage for each element in the matrix
    for (int i = 0; i < T_A.rows(); ++i) {
        for (int j = 0; j < T_A.cols(); ++j) {
            voltage_output(i, j) = calc::normal_distribution(
                    voltage_mean(i, j), voltage_std(i, j));
        }
    }

    return voltage_output;
}

double antenna::antenna_temperature(double received_flux) {
    return received_flux;
}

Eigen::MatrixXd
antenna::gen_terresterial_noise(double effected_freq_channel_by_nbrfi, double nbrfi_bandwidth, double effected_rot_phase_by_bbrfi,
                                       double bbrfi_duration, const std::vector<double>& noise_amplitude_range) {
    auto noise_amplitude_min = min_element(noise_amplitude_range.begin(), noise_amplitude_range.end());
    double min_value = *noise_amplitude_min;
    double nbrfi_amplitude = min_value + (calc::uniform_real_distribution() * fabs(noise_amplitude_range[1] - noise_amplitude_range[0]));
    double bbrfi_amplitude = min_value + (calc::uniform_real_distribution() * fabs(noise_amplitude_range[1] - noise_amplitude_range[0]));

    Eigen::MatrixXd nbrfi_noise = Eigen::MatrixXd::Zero(dataframe.rows(), dataframe.cols()).transpose();
    if (calc::uniform_real_distribution() < prob_nbrfi) {
        nbrfi_noise = gen_noise_NBRFI(effected_freq_channel_by_nbrfi, nbrfi_amplitude, nbrfi_bandwidth);
        nbrfi_count = 1;
    }

    Eigen::MatrixXd bbrfi_noise = Eigen::MatrixXd::Zero(dataframe.rows(), dataframe.cols()).transpose();
    if (calc::uniform_real_distribution() < prob_bbrfi) {
        bbrfi_noise = gen_noise_BBRFI(effected_rot_phase_by_bbrfi, bbrfi_amplitude, bbrfi_duration);
        bbrfi_count = 1;
    }
    return nbrfi_noise + bbrfi_noise;
}

std::pair<Eigen::MatrixXd, Eigen::MatrixXd>
antenna::randomize_antenna_output() {
    double effected_rot_phase_by_bbrfi = rot_phases[rand() % rot_phases.size()];
    double effected_freq_channel_by_nbrfi = freq_channels[rand() % freq_channels.size()];

    Eigen::MatrixXd total_noise = gen_terresterial_noise(effected_freq_channel_by_nbrfi, 0.01, effected_rot_phase_by_bbrfi,
                                                         10,{3, 10});
    is_pulsar_present();
    Eigen::MatrixXd total_flux = dataframe + total_noise.transpose();
    Eigen::MatrixXd total_noisy_output = calculate_antenna_output_voltage_matrix(total_flux);

    return {total_noisy_output, total_flux};
}

void
antenna::is_pulsar_present() {
    // Flatten the dataframe matrix and check the maximum value
    Eigen::MatrixXd dataframe_flattened = dataframe;
    double max_value = dataframe_flattened.maxCoeff();  // Equivalent to np.max(dataframe.flatten())

    // If the max value is less than the sensitivity, return no pulsar
    if (max_value < sensitivity) {
        dataframe = Eigen::MatrixXd::Zero(dataframe.rows(), dataframe.cols());  // Equivalent to np.zeros(shape)
        pulsar_count = 0;  // No pulsar present
    } else {
        pulsar_count = 1;  // Pulsar present
    }
}