import numpy as np

from pulsar_simulation._m_signal_detection_module import randomize_antenna_output
from .information_packet_formats import Payload


class antenna:
    """Digital twin of Antenna"""

    def __init__(self,sensitivity:float=0.1,
                 prob_bbrfi:float=1,
                prob_nbrfi:float=1
                ):
        self.freq_channels = []
        self.sensitivity = sensitivity
        self.prob_nbrfi = prob_nbrfi
        self.prob_bbrfi = prob_bbrfi

    def __call__(self, payload: Payload):
        return randomize_antenna_output(payload.freqs, payload.dataframe, payload.rot_phases,
                                        self.sensitivity, self.prob_nbrfi, self.prob_bbrfi)
