//
// Created by Marcel Trattner on 01.08.24.
//

#include "pulsar_animator.hpp"

pulsar_animator::pulsar_animator(const Eigen::Vector3d& rot_axis, double mag_axis_tilt)
        : __rot_axis(rot_axis), __mag_axis_tilt(mag_axis_tilt) {}

void
pulsar_animator::add_spark(std::vector<shared_ptr<spark_pattern_generator::spark>> spark_gen) { // , SparkSpectralModel spark_spectrum_model = SparkSpectralModel()
    spark_genlist = spark_gen;
    //spark_spectrum_model_list.push_back(spark_spectrum_model);
}

std::vector<Eigen::Vector3d>
        pulsar_animator::operator()(double rot_phase) {
    update_pulsar_state_vectors(rot_phase);
    return mag_frame_basis;
}

void
pulsar_animator::update_pulsar_state_vectors(double rot_phase) {
    Eigen::Vector3d rot_frame_z_axis = __rot_axis;
    Eigen::Vector3d rot_frame_x_axis, rot_frame_y_axis;
    std::tie(rot_frame_x_axis, rot_frame_y_axis) = find_perpendicular_basis_vectors(rot_frame_z_axis);

    mag_frame_basis_att0 = rotate_reference_frame(
            {rot_frame_x_axis, rot_frame_y_axis, rot_frame_z_axis},
            rot_frame_y_axis,
            __mag_axis_tilt * std::numbers::pi / 180.0
    );
    rot_frame_basis = {rot_frame_x_axis, rot_frame_y_axis, rot_frame_z_axis};
    mag_frame_basis = rotate_reference_frame(
            mag_frame_basis_att0,
            rot_frame_z_axis,
            rot_phase * std::numbers::pi / 180.0
    );
}

std::pair<Eigen::Vector3d, Eigen::Vector3d>
pulsar_animator::find_perpendicular_basis_vectors(const Eigen::Vector3d& vector) {
    double a = vector[0], b = vector[1], c = vector[2];
    auto ortho_vector_parametric = [&](double parameter) {
        return Eigen::Vector3d{
                -b * std::cos(parameter) - a * c / std::sqrt(a * a + b * b) * std::sin(parameter),
                a * std::cos(parameter) - b * c / std::sqrt(a * a + b * b) * std::sin(parameter),
                std::sqrt(a * a + b * b) * std::sin(parameter)
        };
    };

    Eigen::Vector3d ortho_vector_x, ortho_vector_y;
    if (a == 0 && b == 0) {
        ortho_vector_x = {1, 0, 0};
        ortho_vector_y = {0, 1, 0};
    } else {
        ortho_vector_x = ortho_vector_parametric(0);
        ortho_vector_y = ortho_vector_parametric(std::numbers::pi / 2);
    }
    return {ortho_vector_x, ortho_vector_y};
}

std::pair<std::vector<double>, std::vector<double>>
pulsar_animator::generate_radio_packet(double rot_phase, const Eigen::Vector3d& direction) {
    return calculate_radio_packet_at_direction(spark_genlist, direction, rot_frame_basis, rot_phase);
}

std::pair<std::vector<double>, std::vector<double>>
pulsar_animator::calculate_radio_packet_at_direction(
        std::vector<shared_ptr<spark_pattern_generator::spark>>& spark_patterns,
        const Eigen::Vector3d& direction,
        const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis,
        double rot_phase) {

    std::vector<double> mid_freq_list, flux_from_sparks_list;
    for (shared_ptr<spark_pattern_generator::spark>& spark : spark_patterns) {
        mid_freq_list.push_back(spark -> get_mid_frequency());
        flux_from_sparks_list.push_back(spark -> apply_physics_model(pulsar_rot_frame_basis, rot_phase, direction));
    }
    return {flux_from_sparks_list, mid_freq_list};
}

payload
pulsar_animator::generate_radio_packet_stream(const std::vector<double>& rot_phases, aggregated_payload& aggregate, const double& scale_direction_randomness) {
    Eigen::Vector3d direction = mag_frame_basis_att0[2];
    // Randomizing direction
    direction[0] = calc::normal_distribution(direction(0), scale_direction_randomness);
    direction[1] = calc::normal_distribution(direction(1), scale_direction_randomness);
    direction[2] = calc::normal_distribution(direction(2), scale_direction_randomness);
    direction.normalize();

    // Add direction to runtime
    aggregate.runtime_params.direction = direction;

    std::vector<double> freq_list;
    for (const auto& spark : spark_genlist) {
        freq_list.push_back(spark -> get_mid_frequency());
    }

    payload payload_obj(freq_list);
    for (const double& rot_phase : rot_phases) {
        payload_obj.add_flux(generate_radio_packet(rot_phase, direction));
    }

    payload_obj.assign_rot_phases(rot_phases);
    return payload_obj;
}

std::vector<Eigen::Vector3d>
pulsar_animator::rotate_reference_frame(
        const std::vector<Eigen::Vector3d>& frame_0_basis,
        const Eigen::Vector3d& axis_of_rotation,
        double rotation_angle) {
    Eigen::AngleAxisd rotation(rotation_angle, Eigen::Vector3d(axis_of_rotation.data()));
    Eigen::Matrix3d rotation_matrix = rotation.toRotationMatrix();

    std::vector<Eigen::Vector3d> frame_1_basis;
    for (const auto& vec : frame_0_basis) {
        Eigen::Vector3d eigen_vec(vec.data());
        Eigen::Vector3d rotated_vec = rotation_matrix * eigen_vec;
        frame_1_basis.push_back({rotated_vec[0], rotated_vec[1], rotated_vec[2]});
    }
    return frame_1_basis;
}