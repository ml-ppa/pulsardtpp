//
// Created by Marcel Trattner on 25.07.24.
//

#ifndef PULSARDTPP_SPARK_PATTERN_GENERATOR_HPP
#define PULSARDTPP_SPARK_PATTERN_GENERATOR_HPP

#include <cmath>
#include <numbers>
#include <numeric>
#include <utility>
#include <vector>
#include <memory>
#include <array>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include "../common/reference_frame_operation.hpp"

using namespace reference_frame_operation;

class spark_pattern_generator {

public:class spark {
private:
    double SPARK_RADIUS_THRESH_FACTOR = 5.0;

    int spark_id;
    double _spark_dimension;
    Eigen::Vector2d _spark_center = {10., 0.};
    Eigen::Vector2d _spark_rotation_axis_polar_att0 = {45., 0.};
    double _drift_vel = -10. / 360.;
    double _drift_phase = 0.;
    double _mid_freq;
    double _flux_power;

    static Eigen::Vector3d ortho_vector_parametric(double a, double b, double c, double parameter);

public:
    spark(
            int spark_id, double spark_dimension, Eigen::Vector2d spark_center,
            Eigen::Vector2d spark_rotation_axis_polar_att0, double drift_vel,  double drift_phase,
            double mid_freq, double flux_power);

    double get_mid_frequency() const { return _mid_freq; };

    double apply_physics_model(const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis, double pulsar_rot_phase, const Eigen::Vector3d& position);
    Eigen::Vector3d calculate_spark_center_att(const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis, double pulsar_rot_phase);
    std::vector<Eigen::Vector3d> calculate_magframe_basis_att(const std::vector<Eigen::Vector3d>& pulsar_rot_frame_basis, double pulsar_rot_phase);
    static std::pair<Eigen::Vector3d, Eigen::Vector3d> find_perpendicular_basis_vectors(const Eigen::Vector3d& vector);
};

public:class spark_spectral_model {

public:
    int model;
    double spectral_index = -0.47;
    double b = 0.21;
    double curvature_parameter = 0.;

    explicit spark_spectral_model(int model = 0) : model(model) {};

    Eigen::VectorXd operator()(const double center_freq, const std::vector<double> frequencies) const;
};

public:

    static vector<shared_ptr<spark_pattern_generator::spark>>
    create_patterned_spark_pattern(const vector<int> &num_sparks, const vector<double> &conal_latitudes,
                                   const Eigen::Vector2d &spark_rotation_axis_polar_att0 = {45, 0}, const double drift_vel = (-10 / 360),
                                   const double spark_dimension = 0.3, const vector<double> &spark_mid_freqs = {},
                                   const vector<double> &spark_flux_powers = {});
};


#endif //PULSARDTPP_SPARK_PATTERN_GENERATOR_HPP
