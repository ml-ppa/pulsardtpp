#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <Eigen/Dense>
#include <vector>


#include "pulsar_simulation/pulsar_animator.hpp"

namespace py = pybind11;


// Function to return three 3D vectors to Python
std::tuple<std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>> 
update_pulsar_state_vectors(Eigen::Vector3d rot_axis, double mag_axis_tilt, double rot_phase) {

    // Update the state of the vectors of the pulsar
    pulsar_animator animator(rot_axis, mag_axis_tilt);
    animator.update_pulsar_state_vectors(rot_phase);

    return std::make_tuple(animator.mag_frame_basis_att0, animator.rot_frame_basis, animator.mag_frame_basis);
}


PYBIND11_MODULE(_m_pulsar_animator, m) {
    // Binding for process_vectors
    m.def("update_pulsar_state_vectors", &update_pulsar_state_vectors, "This function calculates the state vectors of the pulsar",
          py::arg("rot_axis"), py::arg("mag_axis_tilt"), py::arg("rot_phase"));
}