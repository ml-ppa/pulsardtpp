#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <Eigen/Dense>
#include <vector>

#include "data_generator.hpp"
#include "pulsar_simulation/pulsar_animator.hpp"

namespace py = pybind11;

std::pair<Eigen::MatrixXd, Eigen::MatrixXd>
generate_single(const Eigen::Vector3d& rot_axis, double mag_axis_tilt, 
    const std::vector<double>& rot_phases, std::vector<double> freq_channel, std::string tag,
    double signal_time_bin, const std::string& param_folder, const std::string& payload_folder,
    double antenna_sensitivity, double prob_nbrfi, double prob_bbrfi, double scale_direction_randomness,
    bool remove_drift_effect) {

    pulsar_animator anim =  pulsar_animator(rot_axis, mag_axis_tilt);
    data_generator genData = data_generator(anim);
    std::pair<Eigen::MatrixXd, Eigen::MatrixXd> total_noise_total_flux = genData.generate_data_spark_pattern(
            rot_phases, freq_channel, tag, signal_time_bin, param_folder, payload_folder, antenna_sensitivity,
            prob_nbrfi, prob_bbrfi, scale_direction_randomness, remove_drift_effect);

    return total_noise_total_flux;
}


PYBIND11_MODULE(_m_data_generation_module, m) {
    m.def("generate_single", &generate_single, "Routine to generate flux and noisy time frequency graph",
        py::arg("rot_axis"), py::arg("mag_axis_tilt"), py::arg("rot_phases"), py::arg("freq_channel"), 
        py::arg("tag"), py::arg("signal_time_bin"), py::arg("param_folder"), py::arg("payload_folder"),
        py::arg("antenna_sensitivity"), py::arg("prob_nbrfi"), py::arg("prob_bbrfi"), py::arg("scale_direction_randomness"), 
        py::arg("remove_drift_effect"));

}