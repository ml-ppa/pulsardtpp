from skbuild import setup

setup(
    name="pulsardtpp",
    version="0.2",
    description="Tool for analysis and simulation of pulsar data",
    author='PUNCH4NFDI',
    license="GNU",
    packages=['pulsar_simulation'],
    python_requires=">=3.7",
    package_dir = {"": "src"}
)