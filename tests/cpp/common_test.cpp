#include <catch2/catch_all.hpp>
#include <vector>
#include "../../src/common/calculation.hpp"

using namespace calculation;
using namespace std;
/**
 * Test coverage for mathematical helper functions in src/common/calculation.hpp
 */
TEST_CASE( "Cross product (fn: cross_product)", "[Linear Algebra]" ) {
    SECTION("Positive Tests") {
        REQUIRE(cross_product(vector<double>{0.0, 0.0, 0.0}, vector<double>{0.0, 0.0, 0.0}) == vector<double>{0.0, 0.0, 0.0});
        REQUIRE(cross_product(vector<double>{1.0, 2.0, 3.0}, vector<double>{0.0, 0.0, 0.0}) == vector<double>{0.0, 0.0, 0.0});
        REQUIRE(cross_product(vector<double>{1.0, 2.0, 3.0}, vector<double>{2.0, 4.0, 6.0}) == vector<double>{0.0, 0.0, 0.0});
        REQUIRE(cross_product(vector<double>{1.0, 2.0, 3.0}, vector<double>{-1.0, -2.0, -3.0}) == vector<double>{0.0, 0.0, 0.0});
        REQUIRE(cross_product(vector<double>{1.0, 0.0, 0.0}, vector<double>{0.0, 1.0, 0.0}) == vector<double>{0.0, 0.0, 1.0});
        REQUIRE(cross_product(vector<double>{1.0, 2.0, 3.0}, vector<double>{4.0, 5.0, 6.0}) == vector<double>{-3.0, 6.0, -3.0});
        REQUIRE(cross_product(vector<double>{-1.0, -2.0, -3.0}, vector<double>{4.0, -5.0, 6.0}) == vector<double>{-27.0, -6.0, 13.0});
        REQUIRE(cross_product(vector<double>{1.0, 0.0, 0.0}, vector<double>{3.0, 2.0, 4.0}) == vector<double>{0.0, -4.0, 2.0});
    }

    SECTION("Negative Tests") {
        REQUIRE_THROWS(cross_product(vector<double>{0.0, 0.0, 0.0, 0.0}, vector<double>{0.0, 0.0, 0.0}));
        REQUIRE_THROWS(cross_product(vector<double>{0.0, 0.0, 0.0}, vector<double>{0.0, 0.0, 0.0, 0.0}));
        REQUIRE_THROWS(cross_product(vector<double>{0.0, 0.0, 0.0, 0.0}, vector<double>{0.0, 0.0, 0.0, 0.0}));
    }

}

TEST_CASE( "Vector magnitude (fn: vector_magnitude)", "[Linear Algebra]" ) {
    SECTION("Positive Tests") {
        REQUIRE(vector_magnitude(vector<double>{0.0, 0.0, 0.0}) == 0.0);
        REQUIRE(vector_magnitude(vector<double>{1.0, 0.0, 0.0}) == 1.0);
        REQUIRE(vector_magnitude(vector<double>{0.0, 1.0, 0.0}) == 1.0);
        REQUIRE(vector_magnitude(vector<double>{0.0, 0.0, 1.0}) == 1.0);
        REQUIRE(vector_magnitude(vector<double>{-1.0, -2.0, -3.0}) == sqrt(14.0));
        REQUIRE(vector_magnitude(vector<double>{1e6, 1e6, 1e6}) == (sqrt(3) * 1e6));
        REQUIRE(vector_magnitude(vector<double>{1e-6, 1e-6, 1e-6}) == Catch::Approx((sqrt(3) * 1e-6)).margin(1e-10));
        REQUIRE(vector_magnitude(vector<double>{3.0, -4.0, 5.0}) == sqrt(50.0));
        REQUIRE(vector_magnitude(vector<double>{1.0, 1.0, 0.0}) == sqrt(2.0));
    }

    SECTION("Negative Tests") {
        REQUIRE_THROWS(vector_magnitude(vector<double>{0.0, 0.0, 0.0, 0.0}));
        REQUIRE_THROWS(vector_magnitude(vector<double>{0.0, 0.0}));
    }
}

TEST_CASE( "Convolution of two vectors (fn: convolve)", "[Linear Algebra]" ) {
    SECTION("Positive Tests") {
        REQUIRE(convolve(vector<double>{1.0}, vector<double>{2.0}) == vector<double>{2.0});
        REQUIRE(convolve(vector<double>{1.0, 2.0, 3.0}, vector<double>{2.0}) == vector<double>{2.0, 4.0, 6.0});
        REQUIRE(convolve(vector<double>{2.0}, vector<double>{1.0, 2.0, 3.0}) == vector<double>{2.0, 4.0, 6.0});
        REQUIRE(convolve(vector<double>{1.0, 2.0, 3.0}, vector<double>{4.0, 5.0}) == vector<double>{4.0, 13.0, 22.0});
        REQUIRE(convolve(vector<double>{-1.0, -2.0, 3.0}, vector<double>{-4.0, 5.0, -6.0}) == vector<double>{3.0, -16, 27.0});
        REQUIRE(convolve(vector<double>{1.0, 0.0, 0.0, 0.0, 2.0}, vector<double>{0.0, 0.0, 3.0}) == vector<double>{0.0, 3.0, 0.0, 0.0, 0.0});
    }

    SECTION("Negative Tests") {
        REQUIRE_THROWS(convolve(vector<double>{1.0, 2.0, 3.0}, vector<double>{}));
        REQUIRE_THROWS(convolve(vector<double>{}, vector<double>{1.0, 2.0, 3.0}));
    }
}

TEST_CASE( "Fill vector with values increasing in given step (fn: arange)", "[Linear Algebra]" ) {
    SECTION("Positive Tests") {
        REQUIRE(arange(1.0, 10.0, 0.5) == vector<double>{1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5});
        REQUIRE(arange(10.0, 1.0, 0.5) == vector<double>{});
        REQUIRE(arange(10.0, 2.0, -0.5) == vector<double>{10.0, 9.5, 9.0, 8.5, 8.0, 7.5, 7.0 , 6.5, 6.0, 5.5, 5.0, 4.5, 4.0, 3.5, 3.0, 2.5});
        REQUIRE(arange(1.0, 5.0, 8.5) == vector<double>{1.0});
    }

    SECTION("Negative Tests") {

    }
}