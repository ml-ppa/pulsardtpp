Include(FetchContent)

FetchContent_Declare(
        Catch2
        GIT_REPOSITORY https://github.com/catchorg/Catch2.git
        GIT_TAG        v3.4.0 # or a later release
)

FetchContent_MakeAvailable(Catch2)

add_executable(tests
        common_test.cpp
        reference_frame_operation_test.cpp
        data_generator_test.cpp)
target_link_libraries(tests PRIVATE core_library Catch2::Catch2WithMain)

include(Catch)
catch_discover_tests(tests)