//
// Created by Marcel Trattner on 04.11.24.
//

#include <catch2/catch_all.hpp>
#include <Eigen/Dense>
#include <vector>
#include "../../src/common/reference_frame_operation.hpp"

using namespace reference_frame_operation;

TEST_CASE("Rotation of reference frame using quaternion", "[rotate_reference_frame]") {
    // Set up a test reference frame with orthogonal basis vectors
    std::vector<Eigen::Vector3d> frame_0_basis = {
            Eigen::Vector3d(1, 0, 0),
            Eigen::Vector3d(0, 1, 0),
            Eigen::Vector3d(0, 0, 1)
    };

    SECTION("Identity rotation (0 degrees)") {
        Eigen::Vector3d axis_of_rotation(0, 0, 1);
        double rotation_angle = 0.0;  // No rotation

        auto result = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle);

        REQUIRE(result.size() == frame_0_basis.size());
        for (size_t i = 0; i < frame_0_basis.size(); ++i) {
            REQUIRE(result[i].isApprox(frame_0_basis[i], 1e-9));
        }
    }

    SECTION("90-degree rotation around Z-axis") {
        Eigen::Vector3d axis_of_rotation(0, 0, 1);  // Rotate around Z-axis
        double rotation_angle = M_PI / 2;  // 90 degrees in radians

        auto result = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle);

        REQUIRE(result.size() == frame_0_basis.size());

        // Expected results after a 90-degree rotation around Z-axis
        std::vector<Eigen::Vector3d> expected = {
                Eigen::Vector3d(0, 1, 0),
                Eigen::Vector3d(-1, 0, 0),
                Eigen::Vector3d(0, 0, 1)
        };

        for (size_t i = 0; i < frame_0_basis.size(); ++i) {
            REQUIRE(result[i].isApprox(expected[i], 1e-9));
        }
    }

    SECTION("180-degree rotation around Y-axis") {
        Eigen::Vector3d axis_of_rotation(0, 1, 0);  // Rotate around Y-axis
        double rotation_angle = M_PI;  // 180 degrees in radians

        auto result = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle);

        REQUIRE(result.size() == frame_0_basis.size());

        // Expected results after a 180-degree rotation around Y-axis
        std::vector<Eigen::Vector3d> expected = {
                Eigen::Vector3d(-1, 0, 0),
                Eigen::Vector3d(0, 1, 0),
                Eigen::Vector3d(0, 0, -1)
        };

        for (size_t i = 0; i < frame_0_basis.size(); ++i) {
            REQUIRE(result[i].isApprox(expected[i], 1e-9));
        }
    }

    SECTION("45-degree rotation around arbitrary axis") {
        Eigen::Vector3d axis_of_rotation(1, 1, 0);  // Arbitrary axis in the X-Y plane
        double rotation_angle = M_PI / 4;  // 45 degrees in radians

        auto result = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle);

        REQUIRE(result.size() == frame_0_basis.size());

        REQUIRE(result[0].dot(axis_of_rotation.normalized()) == Catch::Approx(frame_0_basis[0].dot(axis_of_rotation.normalized())).epsilon(1e-9));
    }
}

TEST_CASE("Polar to Cartesian conversion", "[polar2cart]") {
    const double tolerance = 1e-9;

    SECTION("Zero radius") {
        double r = 0.0;
        double theta = M_PI / 4; // Arbitrary angle
        double phi = M_PI / 3;   // Arbitrary angle

        Eigen::Vector3d result = polar2cart(r, theta, phi);

        REQUIRE(result.isApprox(Eigen::Vector3d(0, 0, 0), tolerance));
    }

    SECTION("Point at north pole") {
        double r = 5.0;
        double theta = 0.0;
        double phi = M_PI / 4; // Arbitrary angle, should not affect result at pole

        Eigen::Vector3d result = polar2cart(r, theta, phi);
        Eigen::Vector3d expected(0, 0, r);

        REQUIRE(result.isApprox(expected, tolerance));
    }

    SECTION("Point at south pole") {
        double r = 5.0;
        double theta = M_PI;
        double phi = M_PI / 4; // Arbitrary angle, should not affect result at pole

        Eigen::Vector3d result = polar2cart(r, theta, phi);
        Eigen::Vector3d expected(0, 0, -r);

        REQUIRE(result.isApprox(expected, tolerance));
    }

    SECTION("Point on equator, varying azimuth (phi = 0)") {
        double r = 10.0;
        double theta = M_PI / 2; // On the equatorial plane
        double phi = 0.0;

        Eigen::Vector3d result = polar2cart(r, theta, phi);
        Eigen::Vector3d expected(r, 0, 0);

        REQUIRE(result.isApprox(expected, tolerance));
    }

    SECTION("Point on equator, varying azimuth (phi = pi/2)") {
        double r = 10.0;
        double theta = M_PI / 2; // On the equatorial plane
        double phi = M_PI / 2;

        Eigen::Vector3d result = polar2cart(r, theta, phi);
        Eigen::Vector3d expected(0, r, 0);

        REQUIRE(result.isApprox(expected, tolerance));
    }

    SECTION("Point on equator, varying azimuth (phi = pi)") {
        double r = 10.0;
        double theta = M_PI / 2; // On the equatorial plane
        double phi = M_PI;

        Eigen::Vector3d result = polar2cart(r, theta, phi);
        Eigen::Vector3d expected(-r, 0, 0);

        REQUIRE(result.isApprox(expected, tolerance));
    }

    SECTION("General case") {
        double r = 5.0;
        double theta = M_PI / 4; // 45 degrees
        double phi = M_PI / 3;   // 60 degrees

        Eigen::Vector3d result = polar2cart(r, theta, phi);

        Eigen::Vector3d expected(
                r * sin(theta) * cos(phi),
                r * sin(theta) * sin(phi),
                r * cos(theta)
        );

        REQUIRE(result.isApprox(expected, tolerance));
    }
}
