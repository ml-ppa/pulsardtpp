#include <catch2/catch_all.hpp>
#include <Eigen/Dense>
#include <string>
#include "../../src/data_generator.hpp"

TEST_CASE("data_generator::generate_data_spark_pattern - Basic Functionality", "[data_generator]") {
    pulsar_animator anim;
    data_generator generator(anim);

    std::vector<double> rot_phases = calculation::arange(0, 720, 1);
    std::vector<double> freq_channel = calculation::arange(0.5,1.6,0.001);
    std::string tag = "test_tag";
    double signal_time_bin = 0.09166;
    std::string param_folder = "./";
    std::string payload_folder = "./";
    double antenna_sensitivity = 0.5;
    double prob_nbrfi = 0.5;
    double prob_bbrfi = 0.5;
    double scale_direction_randomness = 0.5;
    bool remove_drift_effect = true;

    SECTION("Check output dimensions") {
        auto [total_noise, total_flux] = generator.generate_data_spark_pattern(
                rot_phases, freq_channel, tag, signal_time_bin, param_folder, payload_folder,
                antenna_sensitivity, prob_nbrfi, prob_bbrfi, scale_direction_randomness, remove_drift_effect
        );

        REQUIRE(total_noise.rows() == rot_phases.size());
        REQUIRE(total_flux.rows() == rot_phases.size());
        REQUIRE(total_noise.cols() == freq_channel.size());
        REQUIRE(total_flux.cols() == freq_channel.size());
    }

    SECTION("Validate noise and flux matrices are non-zero") {
        auto [total_noise, total_flux] = generator.generate_data_spark_pattern(
                rot_phases, freq_channel, tag, signal_time_bin, param_folder, payload_folder,
                antenna_sensitivity, prob_nbrfi, prob_bbrfi, scale_direction_randomness, remove_drift_effect
        );

        REQUIRE(total_noise.sum() != Catch::Approx(0.0));
        REQUIRE(total_flux.sum() != Catch::Approx(0.0));
    }
}

TEST_CASE("data_generator::generate_random_spark_pattern - Verify Spark Pattern", "[data_generator]") {
    pulsar_animator anim;
    data_generator generator(anim);

    aggregated_payload aggregate("test_aggregate");

    int num_cones = 3;
    double avg_spark_dimension = 5.0;
    float avg_spark_per_cone_length = 2.5;
    double avg_spark_pattern_center_tilt = 15.0;
    double avg_drift_vel = 1.0;

    auto spark_list = generator.generate_random_spark_pattern(
            aggregate, num_cones, avg_spark_dimension, avg_spark_per_cone_length,
            avg_spark_pattern_center_tilt, avg_drift_vel
    );

    SECTION("Check number of sparks generated") {
        REQUIRE(!spark_list.empty());
        REQUIRE(spark_list.size() > num_cones); // Check if sparks are distributed across cones
    }

    SECTION("Check aggregate runtime parameters") {
        REQUIRE(aggregate.runtime_params.num_sparks.size() == num_cones);
        REQUIRE(aggregate.runtime_params.conal_latitudes.size() == num_cones);
    }
}


TEST_CASE("data_generator::generate_random_ism_obj - Validate ISM Object", "[data_generator]") {
    pulsar_animator anim;
    data_generator generator(anim);
    aggregated_payload aggregate("test_aggregate");

    interstellar_medium ism_obj = generator.generate_random_ism_obj(aggregate);

    SECTION("Check ISM parameters assigned in aggregate") {
        REQUIRE(aggregate.runtime_params.dm_homogeneous == ism_obj.dm_homogeneous);
        REQUIRE(aggregate.runtime_params.scintillation_index_homogeneous == ism_obj.scintillation_index_homogeneous);
    }
}

