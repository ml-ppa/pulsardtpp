import pytest
import numpy as np
import math as mt

from pulsar_simulation.radio_packet_propagation import InterstellarMedium
from pulsar_simulation.information_packet_formats import Payload

# Fixtures for common test data
@pytest.fixture
def default_ism():
    return InterstellarMedium(
        dm_homogeneous=10.0,
        scintillation_index_homo=0.5,
        std_dm=1.0,
        std_scintillation_index=0.1
    )

@pytest.fixture
def payload():
    # Mock Payload class initialization for testing
    payload = Payload(freqs=[1.0, 1.5, 2.0])
    payload.dataframe = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    payload.rot_phases = [0, 45, 90]
    return payload

# Test cases for randomize_props
def test_randomize_props_with_std(default_ism):
    """Test that randomize_props updates dm_homogeneous and scintillation_index_homo"""
    initial_dm = default_ism.dm_homogeneous
    initial_si = default_ism.scintillation_index_homo
    default_ism.randomize_props()

    # Check that properties have potentially changed within reasonable bounds
    assert 0 <= default_ism.dm_homogeneous
    assert initial_dm - 3*default_ism.std_dm <= default_ism.dm_homogeneous <= initial_dm + 3*default_ism.std_dm
    assert 0 <= default_ism.scintillation_index_homo
    assert initial_si - 3*default_ism.std_scintillation_index <= default_ism.scintillation_index_homo <= initial_si + 3*default_ism.std_scintillation_index

def test_randomize_props_without_std():
    """Test randomize_props without std_dm or std_scintillation_index"""
    ism = InterstellarMedium(dm_homogeneous=5.0, scintillation_index_homo=0.5)
    ism.randomize_props()
    # Check that values remain the same as no std was provided
    assert ism.dm_homogeneous == 5.0
    assert ism.scintillation_index_homo == 0.5

# Test cases for propagate_through
def test_propagate_through(payload, default_ism):
    """Test propagate_through to ensure payload is dispersed"""
    signal_time_bin = 1.0
    payload_propagated = default_ism.propagate_through(payload, signal_time_bin)
    
    # Check that output payload has correct data structure
    assert isinstance(payload_propagated, Payload)
    assert payload_propagated.freqs == sorted(payload.freqs, reverse=True), "Frequencies should be reversed"
    assert len(payload_propagated.dataframe) == len(payload.dataframe[0]), "Dataframe should have correct number of rows after transpose"

def test_propagate_through_average(payload, default_ism):
    """Test propagate_through with average_over_period set to True"""
    signal_time_bin = 1.0
    payload_propagated = default_ism.propagate_through(payload, signal_time_bin, average_over_period=True)
    
    # Check payload data structure and length
    assert isinstance(payload_propagated, Payload)
    assert payload_propagated.freqs == sorted(payload.freqs, reverse=True), "Frequencies should be reversed"
    assert len(payload_propagated.dataframe) == len(payload.dataframe[0]), "Dataframe should have correct number of rows after transpose"

# Test cases for calc_disperse_phase_shift
def test_calc_disperse_phase_shift(default_ism):
    """Test calc_disperse_phase_shift for correct phase shift calculation"""
    freq = 1.0  # GHz
    freq_ref = 2.0  # GHz
    signal_time_bin = 1.0  # ms

    phase_shift = default_ism.calc_disperse_phase_shift(freq, freq_ref, signal_time_bin)
    
    # Expected phase shift calculation based on given values
    expected_phase_shift = 4.15 * default_ism.dm_homogeneous * (1 / freq**2 - 1 / freq_ref**2) / signal_time_bin
    assert mt.isclose(phase_shift, expected_phase_shift, rel_tol=1e-6), "Phase shift should match expected calculation"