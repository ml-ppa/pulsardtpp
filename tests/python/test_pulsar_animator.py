import pytest
import numpy as np
from pulsar_simulation.pulsar_animator import PulsarAnimator
from pulsar_simulation.spark_pattern_generator import (
    SparkPatternGenerator,
    SparkSpectralModel,
)


def test_pulsar_animator_initialization():
    # Sample input for initialization
    rot_axis = [0.0, 0.0, 1.0]
    mag_axis_tilt = 45.0
    period = 33.0
    
    # Initialize PulsarAnimator object
    pulsar_animator = PulsarAnimator(rot_axis=rot_axis, mag_axis_tilt=mag_axis_tilt, period=period)
    
    # Check that attributes are correctly initialized
    assert pulsar_animator._PulsarAnimator__rot_axis == rot_axis, "Rotation axis did not initialize correctly"
    assert pulsar_animator._PulsarAnimator__mag_axis_tilt == mag_axis_tilt, "Magnetic axis tilt did not initialize correctly"
    assert pulsar_animator.period == period, "Period did not initialize correctly"
    
    # Check that additional attributes are initialized as None or empty lists
    assert pulsar_animator.mag_frame_basis_att0 is None, "mag_frame_basis_att0 should be None"
    assert pulsar_animator.rot_frame_basis is None, "rot_frame_basis should be None"
    assert pulsar_animator.mag_frame_basis is None, "mag_frame_basis should be None"
    assert pulsar_animator.spark_genlist == [], "spark_genlist should be an empty list"
    assert pulsar_animator.spark_spectrum_model_list == [], "spark_spectrum_model_list should be an empty list"


@pytest.mark.parametrize("rot_axis, mag_axis_tilt, period", [
    ([1.0, 0.0, 0.0], 30.0, 50.0),
    ([0.0, 1.0, 0.0], 60.0, 100.0),
    ([0.0, 0.0, 1.0], 90.0, 33.0)
])
def test_pulsar_animator_different_inputs(rot_axis, mag_axis_tilt, period):
    # Initialize with parameterized inputs
    pulsar_animator = PulsarAnimator(rot_axis=rot_axis, mag_axis_tilt=mag_axis_tilt, period=period)
    
    # Validate inputs
    assert pulsar_animator._PulsarAnimator__rot_axis == rot_axis, f"Expected rot_axis {rot_axis}"
    assert pulsar_animator._PulsarAnimator__mag_axis_tilt == mag_axis_tilt, f"Expected mag_axis_tilt {mag_axis_tilt}"
    assert pulsar_animator.period == period, f"Expected period {period}"


def test_pulsar_animator_default_period():
    # Initialize without specifying period
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=30.0)
    
    # Validate default period
    assert pulsar_animator.period == 33, "Default period should be 33 ms"


def test_empty_spark_list_initially():
    # Ensure lists are empty upon initialization
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=45.0)
    assert pulsar_animator.spark_genlist == [], "spark_genlist should be empty initially"
    assert pulsar_animator.spark_spectrum_model_list == [], "spark_spectrum_model_list should be empty initially"