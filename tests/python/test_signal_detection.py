import pytest
import numpy as np

from pulsar_simulation.signal_detection import antenna
from pulsar_simulation.information_packet_formats import Payload

@pytest.fixture
def sample_payload():
    payload = Payload(freqs=np.linspace(1, 10, 10))
    payload.dataframe = np.random.random((10, 10))
    payload.rot_phases = np.linspace(0, 360, 10)
    return payload

@pytest.fixture
def test_antenna():
    return antenna(sensitivity=0.5, prob_bbrfi=0.5, prob_nbrfi=0.5)

# Test __init__ method
def test_antenna_initialization():
    ant = antenna(sensitivity=0.5, prob_bbrfi=0.8, prob_nbrfi=0.3)
    assert ant.sensitivity == 0.5
    assert ant.prob_bbrfi == 0.8
    assert ant.prob_nbrfi == 0.3
    assert ant.freq_channels == []

# Test __call__ method
def test_antenna_call(test_antenna, sample_payload):
    output = test_antenna(sample_payload)
    assert isinstance(output, tuple) and len(output) == 3, "Output should be a tuple of length 3"
    assert isinstance(output[0], np.ndarray), "First element should be an ndarray (noisy output)"
    assert isinstance(output[1], np.ndarray), "Second element should be an ndarray (total flux)"
    assert isinstance(output[2], dict), "Third element should be a dict (description)"
