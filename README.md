# PulsarDT++

The Square Kilometer Array Observatory (SKAO) will produce so much data that, at best, fractions of it can be archived. A selection of "relevant information" from large data streams will require the development of appropriate analysis pipelines. Existing frameworks in radio astronomy can be used for parallel computing, however not efficiently on more than about 8 nodes. 

The goal of this project is to show that significantly better scaling behavior in radio astronomy is possible, in principle, if the design of a pipeline is optimized. For this reason, our project focuses on a limited task: the identification of pulsar signals during the data acquisition. In this context, an automated detection of radio frequency interference (RFI) signals is crucial.

The current version (0.2) of our framework includes: 
- a layered architecture: a Python-based user interface on the top of a layer with various modules for time-critical analyses (using C++)
- a simplified simulation model of pulsar signals
- parallelization using the python based ray framework

The next steps are a design for a more efficient parallel computing, an improvement of the simulation of pulsar signals, and development of machine learning methods for an automated detection of pulsar signals.

This repository aims to take the simulation from [PulsarDT](https://gitlab.com/ml-ppa/pulsardt) and improve its speed through the performance benefits of C++. For comparison check out the python verison of this project.
 
If you want to run and test the code, we recommend starting with the  [Tutorial Project](https://gitlab.com/ml-ppa/tutorial_project), where you can use a containerized solution for convenience. 

You can also compile and run it locally using virtual environments; therefore, follow the steps explained in the next section. 

### What's new in version 0.2

In this version, we introduce several key updates compared to version 0.1:
* **Spark Pattern Generator**: A new module has been added to create custom spark patterns, inspired by the model proposed by [J.A. Gil and M. Sendyk](https://iopscience.iop.org/article/10.1086/309394/pdf)
* **Optimized Data Generation Pipeline**: The data generation pipeline has been modularized and optimized using Ray, with added functionality to save outputs at various stages for enhanced data reproducibility.
* **Advanced Visualization Tool**: An upgraded visualization tool has been integrated to plot pulsar states using PyVista, providing more detailed and interactive representations.
* **Automated Testing** Ensuring quality control for further iterations.
* **Installable via pip** This tool can easily be installed through the package provided in our registry. Check the [ML-PPA Package Registry](https://gitlab.com/ml-ppa/python-pkgs/-/packages) for instructions.

## Setup to run it locally
Following these steps will enable you to install all necessary dependencies as well as compile and install PularDT++ in an virtual environment. The project has been *tried and tested on Ubuntu 22.04 (x86_64)*, if you are using any other operating system inform yourself how to install the necessary dependencies. From this point you can run the Jupyter Notebooks provided in the [examples](examples/) folder and test the package.

For Ubuntu systems, please install the following dependencies:

```
sudo apt update
sudo apt install git python3.10-venv build-essential python3-dev
```

This project is using several libraries for different tasks like mathematical calculations. In order to retrieve these libraries run the following command when cloning. It is important not to forget the flag **--recurse-submodules** because this project is using several third-party libraries.

```
git clone --recurse-submodules https://gitlab.com/ml-ppa/pulsardtpp.git
cd pulsardtpp
```

We recommend using virtual environment for running this package. This will help organize dependencies and avoid conflics with packages you have installed globally on your computer. You can create a virtual environment with the following command:
```
python3 -m venv <venv_name>
``` 
followed by activating the virtual environment as:
```
source <venv_name>/bin/activate
```
If you have activated the virtual environment, you should see the name <venv_name> at the beginning of the command prompt in your terminal. The required packages or dependencies then can be installed in the virtual environment as:
```
pip install -r requirements.txt
```
the PulsarDT++ Python extension will be comiled and instaled into the currently active python environment with:
```
python setup.py install
```

This will compile the module and package it for Python. It will finalize by installing the package into the currently used Python instance (use of a virtual environment is advised). 

### Debugging the C++ code in an IDE (for development purposes)
This project is built by using CMake, which has been configured for two different build types, depending on how the process is triggered. If the C++ code will be compiled with [scikit-build](https://scikit-build.readthedocs.io/en/latest/index.html), through the script [setup.py](setup.py), then the project will be built as a shared library and inserted into the Python environment. This, however, makes it a bit difficult to debug the C++ code and turns development harder. To solve this problem, the [main.cpp](main.cpp) file has been added to the project and will be built into a pure C++ executable, if CMake is called without the help of scikit. This makes it possible to debug and inspect single functions within the code and also use any IDE that supports CMake.

## Documentation
Further reading on how to use can be found in the project package [PulsarDT](https://gitlab.com/ml-ppa/pulsardt)

## License
Free to use GNU GENERAL PUBLIC LICENSE Version 3.
