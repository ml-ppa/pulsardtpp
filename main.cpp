//
// Created by Marcel Trattner on 27.07.23.
//

/**
 * This file can be used for development, since it can be difficult to debug a Python extension. The CMake file is configured
 * in a way that it will generate an executable from this main file instead of compiling the C++ code into a shared library.
 *
 * Development on the core C++ code can be done with any IDE that supports CMake, to better inspect the correctness of new
 * features.
 */

#include <iostream>
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"

#include "src/common/calculation.hpp"
#include "src/common/structures.hpp"
#include "src/data_generator.hpp"

#include <chrono>
using namespace std::chrono;

using Eigen::MatrixXd;
using namespace rapidjson;

int main()
{
    pulsar_animator anim =  pulsar_animator();

    std::vector<double> rot_phases = calculation::arange(0, 720, 1);
    std::vector<double> freq_channel = calculation::arange(0.5,1.6,0.001);

    // Start measruing time
    auto start = high_resolution_clock::now();

    data_generator genData = data_generator(anim);
    std::pair<Eigen::MatrixXd, Eigen::MatrixXd> total_noise_total_flux = genData.generate_data_spark_pattern(
            rot_phases, freq_channel, "test", 0.09166,"../params/runtime/", "../params/payloads/");

    // Stop measuring time
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    std::cout << "Spark pattern generation finished in: " << duration.count() << " ms." << std::endl;
 }
